\chapter{Experimental Evaluation}%{{{
  Based on our method described so far, we implemented a validity checker for \nuHFLz formula.
  Furthermore, we conducted experiments and compare our tool and existing tools.
  This chapter reports the details of our implementation and the result of the experiments.
\section{Implementation}%{{{
  \subsection{Optimization}%{{{
    We implemented some optimizations for our tool.
  \par
    First, we extended predicate abstraction relation from quadruple to quintuple
      $\AbstTyEnv\ctxSep\predSet\ctxSep\formula\vdash\psi:\aty\absInto\varphi$ by adding
      first-order formula $\formula$.
    The formula $\formula$ means that we can assume $\formula$ holds when we abstract $\psi$.
    For example, \inflabel{A-Pred} is extended as follows.
    \infrule[A-Pred']
      {p(\tilde{a}) \in \predSet}
      {\AbstTyEnv \ctxSep \predSet \ctxSep \formula
          \vdash \pred(\tilde{a}): \tBool
          \absInto b_{(\formula \implies p(\tilde{a}))}}
    Here, predicate $\pred(\tilde{a})$ is modified into $\formula\implies\pred(\tilde{a})$.
    $\inflabel{AC-Base}$ is also extended
    so that the valuation $\rho$ is required to satisfies $\formula$.
    \infrule[AC-Base']
      {\text{$X:\tBool^k\to\tBool \vdash \xi : \tBool^l\to\tBool$} \\
       \eval{X   P_1...P_k}_\rho \sqsupseteq_{\tBool} \eval{\xi Q_1...Q_l}_\rho
          \text{ for all } \rho
          \text{ that satisfies } \formula }
      {\AbstTyEnv \ctxSep \formula \vdash \varphi
          :        (P_1,...,P_k, \tBool)
          \subType (Q_1,...,Q_l, \tBool)
          \absInto [\lambda b_{P_1}...b_{P_k}.\varphi/X] \xi b_{Q_1}...b_{Q_l}}
    % Therein, $\rho$ is required to satisfy $\formula$.
    The formula $\formula$ is refined in conjunctions and disjunctions. For example,
    the following rule is added.
    \infrule[A-OrLeftFO]
      {\psi_1 \text{ is a first-order formula} \andalso
       \AbstTyEnv \ctxSep \predSet \ctxSep \formula \land \neg\psi_1
          \vdash \psi_2 : \tBool
          \absInto \varphi}
      {\AbstTyEnv \ctxSep \predSet \ctxSep \formula
          \vdash \psi_1\lor\psi_2
          \absInto \varphi}
    This rule means that in the abstraction of $n \le 0 \lor \psi$,
    we can assume $n > 0$ in the abstraction of $\psi$.
    This optimization decreases the number of predicates and thus
      improves the performance of predicate abstraction.
  \par
    Second, we bounded the maximum number of conjunctions and disjunctions in the
    $\xi$ of the \inflabel{AC-Base} rule since it is quite expensive to compute the best one.
  %}}}
  \subsection{Backend Tools}%{{{
    Our implementation is written in OCaml and depends on some tools during predicate abstraction,
    \nuHFL validity checking, and predicate discovery in CEGAR.
    This section describes what tools we used for each task.
  \par
    In predicate abstraction, we used Z3~\cite{Z3} as an SMT solver
      to compute $\xi$ in \inflabel{AC-Base}.
  \par
    In validity checking of \nuHFL formula, we used a HORS model checker named HorSat2.
    Although there is a (full) \HFL model checker developed by Hosoi et al.~\cite{PureHflMC},
    it is better to adapt HorSat2 using
      reduction~\cite{KobayashiESOP18,WatanabePEPM19} from an \HFL validity checking problem to a HORS model checking problem
    because
      (i)  the reduction is quite easy in $\nu$-only setting and
      (ii) HorSat2 can output more simple counterexamples than Hosoi's tool
           since the target class of HorSat2 is a strict subset (modulo the reduction)
           of full \HFL model checking\footnote{
             Full \HFL model checking problem corresponds to
             parity automata HORS model checking problem.
             HorSat2 is a HORS model checker for (co)trivial automata, which is a subclass
             of parity automata.
           }.
  \par
    In predicate discovery, we used CHC solvers RCaml~\cite{RCaml} and HoIce~\cite{HoIce} to solve
    the constraints generated for typing SHP in Section~\ref{subsection:typing_SHP}.
    Among the many existing CHC solvers, we chose these solvers because
    RCaml is designed to find good predicates for verification of higher-order functional programs and
    HoIce is reported to be
      good at finding simple solutions~\cite{HoIce} and
      effective in higher-order program verification~\cite{PEPM19Sato}.
    As reported by Suzuki~\cite{Suzuki}, we can take advantage of them since \nuHFLz
    highly resembles higher-order functional programs.
  \par
    Versions of these software used in this experiment are shown in Table~\ref{experiment:versions}.
    \begin{table}[ht!] %{{{ num
      \begin{center}
        \begin{tabular}{ll}
        Name of software  & Version \\ \hline
        OCaml             & 4.08.1  \\
        Z3                & 4.7.1   \\
        HorSat2           & 0.94    \\
        RCaml             & 9772c64 (commit hash)\\
        HoIce             & 1.81
        \end{tabular}
      \end{center}
      % TODO hoiceを修正した件
      % TODO RCamlではなくmochiのバージョンだなこれ
      \caption{Comparison with Horus: number of instance solved}
      \label{experiment:versions}
    \end{table} %}}}
  \subsubsection{Reduction of Out Constraints to CHC}
    Note that the set of constraints generated for typing SHP is not CHC\@.
    To make use of CHC solvers, we reduced the constraint solving problem into a
    sequence of CHC solving problem by the following procedure.
    For simplicity, assume that the constraints are in the following form.
    This is not a CHC because it has a disjunction in the head of the first constraint.
    \begin{align*}
      \mathrm{P}_1(\tilde{x}) \lor \mathrm{P}_2(\tilde{y}) &\Leftarrow \formula \\
      \formula_1 &\Leftarrow \mathrm{P}_1(\tilde{x}) \land \formula_1' \\
      \formula_2 &\Leftarrow \mathrm{P}_2(\tilde{y}) \land \formula_2'
    \end{align*}
    Because the constraints are acyclic, however, we can compute the upper bound $\bar{\mathrm{P}}_2(\tilde{y})$ of
    $\mathrm{P}_2$. If the above constraints are satisfiable, so is the following CHC.
    \begin{align*}
      \mathrm{P}_1(\tilde{x}) &\Leftarrow \formula \land \neg\bar{\mathrm{P}}_2(\tilde{y})  \\
      \formula_1 &\Leftarrow \mathrm{P}_1(\tilde{x}) \land \formula_1'
    \end{align*}
    Let $P^*_1(\tilde{x})$ be a solution of this CHC\@. Using $P^*_1(\tilde{x})$, we construct another CHC:
    \begin{align*}
      \mathrm{P}_2(\tilde{y}) &\Leftarrow \formula \land \neg\mathrm{P}^*_1(\tilde{x})  \\
      \formula_2 &\Leftarrow \mathrm{P}_2(\tilde{y}) \land \formula_2'
    \end{align*}
    This CHC is also satisfiable. Let $P^*_2(\tilde{y})$ be a solution.
    Then, the pair of $\mathrm{P}^*_1(\tilde{x})$ and $\mathrm{P}^*_2(\tilde{y})$ is a solution of original constraints.
    %}}}
%}}}
\section{Experiment} %{{{
    We conducted experiments to compare our tool and those from two from related work.
    One is Suzuki's tool, which is already described in detail in previous chapters.
    The other is Horus developed by Burn et al.~\cite{Horus} that solves the satisfiability problem of
    higher-order constrained Horn clause, which is the dual problem of \nuHFLz validity checking
    problem.
    Both experiments ran on an Intel Core i7-8550U CPU machine with 8GB ram, running Linux.
  \subsection{Comparison with Suzuki's tool} %{{{
    The first experiment compares ours and Suzuki's tool.
    As a benchmark set, we used 148 test cases written by them.
    Each test case is obtained by a reduction of Kobayashi et al.~\cite{KobayashiESOP18}
    from an instance of a functional program verification problem used in the benchmark of~\cite{MoCHi}.
    138 of them originate from verification problems of safety property and
     10 from that of non-termination property.
    Since some of the instances are not written in HES style, we rewrite it in HES style
      without changing the meaning of the formula.
  \par
    The result of the experiment is described in Table~\ref{experiment:num} and Figure~\ref{experiment:speed}.
    Table~\ref{experiment:num} shows how many instances each tool can or cannot solve.
    Here, `Timeout' means that it took more than 180 sec before it terminates and `Error' means
      failure due to incompleteness of refinement type system for non-closed fixpoint free \nuHFLz\footnote{
        Indeed, most instances of benchmark of Suzuki have free variables which are intended to be interpreted as universally quantified.
        In the presence of free variables, the refinement type system is not complete even if the formula is limited to SHP\@.
        } (common 1 case),
      failure to find new predicate (4 cases of Suzuki's), or
      other unknown error (3 cases of Suzuki's).
    %   For safety property, our tool can solve 124 more instances than Suzuki's.
    %   The number of error (explained later) is also less than Suzuki's.
    %   On the other hand, for non-termination property, Suzuki's tool can solve more problems.
    %   In total, our tool can solve more instances with less errors.
    Figure~\ref{experiment:speed} shows how long each tool took to solve instances.
    In most instances, Suzuki's tool outperformed ours.
    \begin{table}[ht!] %{{{ num
      \begin{center}
        \begin{tabular}{ll|lll} % hoice mergeを切った
                                                       &         & safety & non-termination & total \\ \hline
        \multicolumn{1}{l|}{\multirow{3}{*}{Ours}}     & Solved  &    124 &               4 &   127 \\
        \multicolumn{1}{l|}{}                          & Timeout &     14 &               6 &    20 \\
        \multicolumn{1}{l|}{}                          & Error   &      1 &               0 &     1 \\ \hline
        \multicolumn{1}{l|}{\multirow{3}{*}{Suzuki's}} & Solved  &    114 &              10 &   124 \\
        \multicolumn{1}{l|}{}                          & Timeout &     16 &               0 &    16 \\
        \multicolumn{1}{l|}{}                          & Error   &      8 &               0 &     8 \\
        \end{tabular}
      \end{center}
      \caption{Comparison with Suzuki's tool on the number of instance solved}
      \label{experiment:num}
    \end{table} %}}}
    \begin{figure}[ht!] %{{{ speed
      \begin{center}
        \includegraphics[width=0.8\textwidth]{./Experiment/result-suzuki}
      \end{center}
    \caption{Comparison with Suzuki's tool}
    \label{experiment:speed}
    \end{figure} %}}} speed
  %}}}
  \subsection{Comparison with Horus} %{{{
    The second experiment compares ours and Horus~\cite{Horus},
    a higher-order constrained Horn clause (HOCHC) satisfiability solver.
    \nuHFLz validity checking problem and HOCHC satisfiability checking problem are equivalent
    in the sense that instances of them can be reduced into each other by negating the formulas or constraints.
    Horus reduces the problem into (first-order) CHC satisfiability checking problem so that
    if the reduced problem is satisfiable so is the original. Note that the inverse does not hold:
    if the CHC is satisfiable, it does not say anything about the original HOCHC.
    Thus, Horus cannot prove the satisfiability of HOCHC (or disprove the validity of \nuHFLz formula),
    unlike ours and Suzuki's tool.
  \par
    We used two sets of test cases in this experiment. Both sets are obtained by translating
    instances used in the benchmark of~\cite{MoCHi} which is also used in the experiment with Suzuki's tool.
    The difference is that the set (I) is written by the authors of Horus while the set (II) is
    a subset of the test cases written by Suzuki used in the comparison with their tool.
    All instances are unsatisfiable HOCHC (or valid \nuHFLz formula) because of the
    above restriction of Horus\@. We used Z3 as a backend CHC solver of Horus\@.
  \par
    The result is shown in Table~\ref{experiment:horus}.
    In the table, `Error' and `Timeout' is the same as the first experiment and `Failed' means that
    the reduced CHC is satisfiable. In the point of view of performance, Horus outperformed ours
    in all solved instances. Indeed, Horus terminated within 0.1 seconds in most solved instances.
    \begin{table}[ht!] %{{{ num
      \begin{center}
        \begin{tabular}{ll|ll}                      &         & set (I) & Set (II)  \\ \hline
        \multicolumn{1}{l|}{\multirow{3}{*}{Ours}}  & Solved  &     10  &        67  \\
        \multicolumn{1}{l|}{}                       & Timeout &      0  &         3  \\
        \multicolumn{1}{l|}{}                       & Error   &      1  &         1  \\ \hline
        \multicolumn{1}{l|}{\multirow{3}{*}{Horus}} & Solved  &      9  &        40  \\
        \multicolumn{1}{l|}{}                       & Timeout &      0  &         3  \\
        \multicolumn{1}{l|}{}                       & Failed  &      1  &        35  \\
        \end{tabular}
      \end{center}
      \caption{Result of comparison with Horus}
      \label{experiment:horus}
    \end{table} %}}}
  %}}}
  \subsection{Analysis on Result} %{{{
    On comparison with Suzuki's tool, the experiment shows that
    our tool solved more instances for safety property and fewer instances for non-termination property.
    The reason why it is not good at solving instances of non-termination property is that
    the instance has an untypical form since infinite branching in the original instance
    of Kobayashi et al~\cite{MoCHi} is replaced by finite one by Suzuki,
    due to the lack of expression power of \nuHFLz\footnote{
      We need the least fixpoint operater to express the existensial quantifier that ranges over integers.
    }:
    the expression $\mathtt{let}\; x = \mathtt{rand\_int}()\;\mathtt{in}\;e$ is translated into
    $\mathtt{Exists'}\;(\lambda x. \eval{e})$ where $\eval{e}$ denotes the reduction and
    $\mathtt{Exists'}$ is defined by the following formula.
    \begin{align*}
      \mathtt{Exists'} \equiv \lambda p.\; p(-3) \lor p(3) \lor p(5) \lor p(8) \lor p(10)
    \end{align*}
    Our tool cannot handle this sequence of disjunctions well because it increases the number of predicates.
    We believe our tool generally solves more instances than Suzuki's tool for usual formulas.
    Notably, Suzuki's tool failed to find new predicates because of the obvious lack of
    progress property due to the restriction on the branching condition while ours did not raise
    this type of error.
    In performance, Suzuki's tool outperformed our tool. However, This is an expected result since
    our coercion rule is more precise at the cost of more large search space.
  \par
    In the second experiment, it turned out that Horus runs far faster than ours.
    though the number of solved instances is small especially for the set (II).
  %}}}
%}}}
%}}}
