\chapter{Preliminaries}\label{chapter:Preliminaries}

This chapter reviews the definition of \nuHFL and \nuHFLz.
First, we introduce a general notations.

\begin{Notation}
  We write $\tilde{v}$ for a possibly empty sequence $v_1,...,v_n$.
  By $\varepsilon$, we denote the empty sequence.
  We write $\dom{f}$ for the domain of map $f$.
  A map $f$ is represented by a set of bindings of the form $x \mapsto v$:
    $\{ x_1 \mapsto v_1, ..., x_n \mapsto v_n\}$ denotes the map $f$ such that
    $\dom{f} = \{x_1,...,x_n\}$ and $f(x_i) = v_i$ for each $i \in \{1,...,n\}$.
  When $\dom{f} \cap \dom{g} = \emptyset$, $f \cup g$ denotes the map $h$ such that
    $\dom{h} = \dom{f}\cup\dom{g}$ with
    $\forall x \in \dom{f}. h(x) = f(x)$ and
    $\forall x \in \dom{g}. h(x) = g(x)$.
  We write $[t/x]t'$ to denote substitution, or replacing all free occurrences of $x$ in $t'$ with $t$.
  % We write $f \sqcup [ x \mapsto v]$ to denote the map $g$ such that
  %   $\dom{g} = \dom{g}\cup\{x\}$ and $\forall y \in \dom{f}\setminus\{x\}. g(y) = f(y)$ and $g(x) = v$.
  % $A \lhd f$ and $A \dsubt f$ denotes the domain restriction and domain substraction respectively.
  % For example, $(\{x\} \dsubt f) \cup \{ x \mapsto v \}$ denotes the map $g$ such that
  %   $g(x) = v$ and $g(y) = f(y)$ for all $y \in \dom{f} \setminus \{x\}$.
\end{Notation}

\begin{Definition}[\nuHFLz]
  A \nuHFLz formula $\psi$ and arithmetic formula $a$ is defined by following grammars.
  \begin{alignat*}{3}
    &(\text{formula})\quad&
      \psi   &::= x^\tau \mid \true \mid \false \mid \psi_1\lor\psi_2 \mid \psi_1\land\psi_2 \\
             &&&\phantom{:=}\mid
                  \lambda x^{\ex{\sty}}.\psi \mid \psi \ex{\psi} \mid \nu x^\sty .\psi \mid
                  \pred(\tilde{a})  \\
    &(\text{arithmetic formula})\quad&
      a      &::= n \mid x^\tInt \mid \op(\tilde{a}) \\
    &(\text{extended formula})\quad&
      \ex{\psi} &::= \psi \mid a \\
    &(\text{type})\quad&
      \sty &::= \tBool \mid \ex{\sty}\to\sty \\
    &(\text{extended type})\quad&
      \ex{\sty} &= \sty \mid \tInt
    % &(\text{environment})\quad&
    %   \SimpleTyEnv &::= \varepsilon \mid \SimpleTyEnv,x:\ex{\sty}
  \end{alignat*}
  Therein $x$ is a variable, $n$ is an integer. The construct $\pred(\tilde{a})$ is a predicate on
  integers such as $a_1 \le a_2$, and $\op(\tilde{a})$ is an operation such as addition or subtractions.
  Every formula $\psi$ has a simple type $\sty$. The type $\tBool$ means the Boolean type (or the type of propositions).
  Note that the final return type of $\sty$ is always $\tBool$. Thus, for example, $\tInt\to\tInt$ is an invalid type.
  We often omit the type annotation on the shoulder of variables.
  % As usual, we restrict the syntax of formulas using a simple type system.
  We use the metavariable $\SimpleTyEnv$ to denote a type environment or a map from variables to extended types.
  The typing relation
    $\SimpleTyEnv \vdash_\mathrm{ST} \ex{\psi} : \ex{\sty}$
  defined in Figure \ref{rules:simple_typing} is the same as that of the standard simply typed lambda
  calculus extended with fixpoints. Henceforth, we consider only well-typed formulas.
  % \TODO{直観の説明}
\par
  For each extended simple type $\ex{\sty}$, the semantic domain $\Domain{\ex{\sty}}$ and order $\sqsubseteq_{\ex{\sty}}$ on it is defined.
  For non-extended simple type $\sty$, $(\Domain{\sty}, \sqsubseteq_\sty)$ forms a complete lattice.
  \begin{align*}
    \Domain{\tBool}    &= \{\vtrue, \vfalse\} &
    \sqsubseteq_\tBool &= \{ (v_1,v_2) \mid \models v_1 \Rightarrow v_2 \}
    \\
    \Domain{\tInt}    &= \mathbb{Z} &
    \sqsubseteq_\tInt &= \{ (n,n) \mid n \in \mathbb{Z} \} 
    \\
    \Domain{\ex{\sty}\to\sty}      &= 
        \{ f \in \Domain{\ex{\sty}}\to\Domain{\sty} \mid \text{$f$ is monotonic} \} &
    \sqsubseteq_{\ex{\sty}\to\sty} &=
        \{ (f,g) \mid \forall v\in\Domain{\ex{\sty}}. f(v)\sqsubseteq_{\sty} g(v) \}
  \end{align*}
  Assume $\SimpleTyEnv\vdash_\mathrm{ST} \ex{\psi}:\ex{\sty}$.
  Given valuation $\rho$ which respects $\SimpleTyEnv$
    (i.e, $\rho(x) \in \Domain{\ex{\sty}}$ for all $x:\ex{\sty} \in \SimpleTyEnv$),
    the semantic value
      $\eval{\SimpleTyEnv\vdash_\mathrm{ST}\ex{\psi} : \ex{\sty}}_\rho$
    of $\psi$ is defined by following.
  \begin{align*}%{{{
    \eval{\SimpleTyEnv, x:\sty \vdash_{\mathrm{ST}} x:\sty}_\rho &= \rho(x)
      \\
    \eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \true:\tBool}_\rho &= \vtrue
      \\
    \eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \false:\tBool}_\rho &= \vfalse
      \\
    \eval{\SimpleTyEnv \vdash_{\mathrm{ST}} n:\tInt}_\rho &= n
      \\
    \eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \op(a_1,...,a_n): \tInt}_\rho &=
        \eval{\op}(\eval{\SimpleTyEnv\vdash_\mathrm{ST} a_1:\tInt}_\rho,...,
                   \eval{\SimpleTyEnv\vdash_\mathrm{ST} a_n:\tInt}_\rho)
      \\
    \eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \pred(a_1,...,a_n): \tBool}_\rho &=
        \eval{\pred}(\eval{\SimpleTyEnv\vdash_\mathrm{ST} a_1:\tInt}_\rho,...,
                     \eval{\SimpleTyEnv\vdash_\mathrm{ST} a_n:\tInt}_\rho)
      \\
    \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi\ex{\psi} : \sty}_\rho &=
       \eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \psi: \ex{\sty}\to\sty}_\rho
      (\eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \ex{\psi}: \ex{\sty}}_\rho)
      \\
    % \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi_1\psi_2 : \sty_2}_\rho &=
    %   \eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \psi_1 : \sty_1\to\sty_2}_\rho
    %   (\eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \psi_2: \sty_1}_\rho)
    %   \\
    % \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi a : \sty}_\rho &=
    %   \eval{\SimpleTyEnv \vdash_{\mathrm{ST}} \psi : \tInt\to\sty}_\rho
    %   (\eval{\SimpleTyEnv \vdash_{\mathrm{ST}} a : \tInt}_\rho)
    %   \\
    \eval{\SimpleTyEnv \vdash_\mathrm{ST} \lambda x^{\ex{\sty}}. \psi : \ex{\sty}\to\sty}_\rho &=
      \{ v \mapsto \eval{\SimpleTyEnv,x:\ex{\sty} \vdash_\mathrm{ST} \psi}_{\rho\cup\{x\mapsto v\}}
      \mid v \in \Domain{\sty_1} \}
      \\
    % \eval{\SimpleTyEnv \vdash_\mathrm{ST} \lambda x. \psi : \tInt\to\sty}_\rho &=
    %   \{ v \mapsto \eval{\SimpleTyEnv,x:\tInt \vdash_\mathrm{ST} \psi}_{\rho\cup\{x\mapsto v\}}
    %   \mid v \in \Domain{\tInt} \}
    %   \\
    % \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi_1\land\psi_2 : \tBool}_\rho &=
    %   \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi_1 : \tBool}_\rho \sqcap
    %   \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi_2 : \tBool}_\rho
    %   \\
    % \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi_1\lor\psi_2 : \tBool}_\rho &=
    %   \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi_1 : \tBool}_\rho \sqcup
    %   \eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi_2 : \tBool}_\rho
    %   \\
    \eval{\SimpleTyEnv \vdash_\mathrm{ST} \nu x.\psi : \sty}_\rho &=
      \mathrm{gfp}(\eval{\SimpleTyEnv \vdash_\mathrm{ST} \lambda x. \psi : \sty }_\rho)
  \end{align*}%}}}
  Therein, $\eval{\op}$ (resp. $\eval{\pred}$) is a function denoted by $\op$ (resp. $\pred$) and
    $\mathrm{gfp}(f)$ is the greatest fixpoints\footnote{
      The greatest fixpoint always exists because $\Domain{\sty}$ is a complete lattice.
    } of $f$.
  We say $\psi$ is valid if
    $\eval{\SimpleTyEnv \vdash_\mathrm{ST} \psi : \tBool}_\rho$ is $\vtrue$
    for any valuation $\rho$.
  We often omit $\SimpleTyEnv$ and $\sty$ if they are obvious from context.
  For closed \nuHFLz formula $\psi$, we simply write $\eval{\psi}$ instead of
    $\eval{\emptyset \vdash_\mathrm{ST} \psi : \tBool}_\emptyset$.
  In this thesis, we usually focus on the validity of closed formulas.
\end{Definition}

\begin{Example}\label{example:2:HFL}
  Let $\psi$ be $\nu \mathrm{X}.\lambda n.\; n = 0 \lor (n > 0 \land \mathrm{X}(n-2))$.
  Then $\models \psi(n)$ holds if and only if $n$ is a non-negative even number.
\end{Example}
\begin{Remark}
  \nuHFLz formula can be regarded as a tree-generating grammar which has 
  $\land$ or $\lor$ as its node and $\pred(\tilde{a})$ as its leaf.
  For example, $\psi(n)$ denotes the following tree.
  \begin{align*}%{{{
    \text{
      \Tree[
        .$\lor$
          [ .$n=0$ ]
          [ .$\land$
            [ .$n>0$ ]
            [ .$\lor$
              [ .$n=2$ ]
              [ .$\land$
                [ .$n>2$ ]
                [ .$\dots$ ]
                % [ .$\lor$
                %   [ .$n=4$ ]
                %   [ .$\land$
                %     [ .$n>4$ ]
                %     [ .$\dots$ ]
                %   ]
                % ]
              ]
            ]
          ]
      ]
    }
  \end{align*}%}}}
  For \nuHFLz, infinite paths are considered as \emph{true}.
  Let $\psi'$ be $\nu \mathrm{X}.\lambda n.\; n = 0 \lor \mathrm{X}(n-2)$, which denote
  the following tree.
  \begin{align*}%{{{
    \text{
      \Tree[
        .$\lor$
          [ .$n=0$ ]
          [ .$\lor$
            [ .$n=2$ ]
            [ .$\lor$
              [ .$n=4$ ]
              [ .$\dots$ ]
            ]
          ]
      ]
    }
  \end{align*}%}}}
  Then $\psi'(n)$ is valid for any integer $n$ because of the right-most path.
\end{Remark}

\begin{figure}[ht!] %{{{ Simple typing
  \begin{gather*}
    \infer[\inflabel{S-Var}]
      {\SimpleTyEnv, x:\sty \vdash_{\mathrm{ST}} x:\sty}
      {}
      \andalso
    \infer[\inflabel{S-Bool}]
      {\SimpleTyEnv \vdash_{\mathrm{ST}} b: \tBool}
      {b \in \{\true,\false\}}
      \andalso
    \infer[\inflabel{S-Int}]
      {\SimpleTyEnv \vdash_\mathrm{ST} n : \tInt}
      {}
      \\
    \infer[\inflabel{S-Pred}]
      {\SimpleTyEnv \vdash_{\mathrm{ST}} \pred(a_1,...,a_n): \tBool}
      {\SimpleTyEnv \vdash_\mathrm{ST} a_i : \tInt \text{ for each $i$}}
      \andalso
    \infer[\inflabel{S-Op}]
      {\SimpleTyEnv \vdash_\mathrm{ST} \op(a_1,...,a_n) : \tInt}
      {\SimpleTyEnv \vdash_\mathrm{ST} a_i : \tInt \text{ for each $i$}}
      \\
    \infer[\inflabel{S-App}]
      {\SimpleTyEnv
          \vdash_\mathrm{ST} \psi\ex{\psi} : \sty}
      {\SimpleTyEnv
          \vdash_{\mathrm{ST}} \psi : \ex{\sty}\to\sty \andalso
       \SimpleTyEnv \vdash_{\mathrm{ST}} \ex{\psi}: \ex{\sty}}
      \andalso
    \infer[\inflabel{S-Abs}]
      {\SimpleTyEnv
          \vdash_\mathrm{ST} \lambda x^{\ex{\sty}}. \psi : \ex{\sty}\to\sty}
      {\SimpleTyEnv,x:\ex{\sty}
          \vdash_\mathrm{ST} \psi : \sty}
      \\
    \infer[\inflabel{S-And}]
      {\SimpleTyEnv
          \vdash_\mathrm{ST} \psi_1\land\psi_2 : \tBool}
      {\SimpleTyEnv
          \vdash_\mathrm{ST} \psi_1 : \tBool \andalso
       \SimpleTyEnv
          \vdash_\mathrm{ST} \psi_2 : \tBool}
      \andalso
    \infer[\inflabel{S-Or}]
      {\SimpleTyEnv
          \vdash_\mathrm{ST} \psi_1\lor\psi_2 : \tBool}
      {\SimpleTyEnv
          \vdash_\mathrm{ST} \psi_1 : \tBool \andalso
       \SimpleTyEnv
          \vdash_\mathrm{ST} \psi_2 : \tBool}
      \\
    \infer[\inflabel{S-Nu}]
      {\SimpleTyEnv \vdash_\mathrm{ST} \nu x^\sty.\psi : \sty}
      {\SimpleTyEnv,x:\sty \vdash_\mathrm{ST} \psi : \sty }
  \end{gather*}
\caption{Simple typing of \nuHFLz}
\label{rules:simple_typing}
\end{figure} %}}} End of Simple typing

Up to here, we explained the standard definition of \nuHFLz.
For discussing predicate abstraction, it is convenient to introduce another notation of \nuHFLz,
 called hierarchical equation system (HES), first introduced by Kobayashi et al.~\cite{HORS_HFL}.

\begin{Definition}[HES]
  A hierarchical equation system (HES) $D$ is a sequence of equations of the form
  $F_0^{\sty_0} =_\nu \psi_0;... ;F_n^{\sty_n} =_\nu \psi_n$
  where, for each $i$, $\psi_i$ is a formula without fixpoints such that
  $F_0 : \sty_0,..., F_n : \sty_n \vdash_\mathrm{ST} \psi_i : \sty_i$.
\end{Definition}
Again, we often omit type annotations.
The translation from HES to standard (closed) \nuHFLz formula is defined by
$\FromHES{\_}$ function below.
\begin{align*}
  \FromHES{X^\sty =_\nu \psi}    &= \nu X. \psi. \\
  \FromHES{D; X^\sty =_\nu \psi} &= \FromHES{[\nu X^\sty. \psi/ X] D}
\end{align*}
The semantics of HES is given via this function.
The inverse translation can be done by using the same technique as lambda-lifting.
It is known that $\eval{\FromHES{D}}_\emptyset$ is equal to $\rho^*(F_0)$ where
$\rho^*$ is the greatest fixpoint of the following function $f_D$:
  $f_D(\rho) = \{ X \mapsto \eval{\psi}_\rho \mid X^\sty=_\nu \psi \in D\}$.
Therefore, the order of definition in the HES does not matter except the first one
($F_0 = \psi_0$) in \nuHFLz\footnote{
  In the full \HFLz, which allows alternations of least and greatest fixpoints,
  the semantics of formula depends on the order of definitions.
}.
We write the first symbol ($F_0$) as $S_D$.

\begin{Example}\label{example:2:HES}
  The \nuHFLz formula in Chapter~\ref{chapter:Introduction} is formally defined by
  following HES\@.
  \begin{align*}
    \mathrm{S} &=_\nu \mathrm{Forall}\;0\;
      (\lambda x. \mathrm{NonNegative}\;x \lor \mathrm{NonPositive}\;x); \\
    \mathrm{Forall} &=_\nu \lambda n. \lambda p.\;
      p\;n \land \mathrm{Forall}\;(n-1)\;p \land \mathrm{Forall}\;(n+1)\;p;\\
    \mathrm{NonNegative} &=_\nu \lambda n.\;n \ge 0;\\
    \mathrm{NonPositive} &=_\nu \lambda n.\;n \le 0
  \end{align*}
\end{Example}
We can restrict HES so that $\lambda$ occurs only at the top level. For example,
the above $\mathrm{S}$ can be transformed into
  $\mathrm{S} =_\nu \mathrm{Forall}\;0\;F;\;
   \mathrm{F} =_\nu \lambda x.\;\mathrm{NonNegative}\;x \lor \mathrm{NonPositive}\;x$.

At last, we will introduce \nuHFL, the result of predicate abstraction which does not contain 
arithmetic expressions.

\begin{Definition}[\nuHFL]
  \nuHFL formula $\varphi$ is defined by the following grammar.
  \begin{align*}
    \varphi   &::= x^\sty \mid \true \mid \false \mid \varphi_1\lor\varphi_2 \mid \varphi_1\land\varphi_2
           \mid \lambda x^\sty.\varphi \mid \varphi_1\varphi_2 \mid \nu x^\sty.\varphi
  \end{align*}
  Here, the type annotation $\sty$ in $x^\sty$ does not
  contain the type $\tInt$.
  The typing rule and semantics are defined by the same rules as \nuHFLz.
  The HES syntax is also defined for \nuHFL.
\end{Definition}

It is known that the validity checking of closed \nuHFL formula is decidable~\cite{HFL}.

