\chapter{Soundness of refinement type system}\label{appendix:soundness} %{{{
\newcommand{\MinVal}[1]{\llparenthesis#1\rrparenthesis}
\begin{Definition}[$\MinVal{\rty}$]%{{{
  For a closed refinement type $\rty$ with $\vdash \rty :: \sty$,
  a value $\MinVal{\rty} \in \Domain{\sty}$ is defined by following equations.
  Intuitively, $\MinVal{\rty}$ is the minimum value which has type $\rty$ as shown by
  Lemma \ref{lem:soundness_of_ref}.
  \begin{align*}
    \MinVal{\tBool} &=
      \vtrue \\
    \MinVal{x:\formula\to\rty} &=
      \left\{ n \mapsto
        \begin{cases}
          \MinVal{\rty_{\{x\mapsto n\}} }
                & \text{if $\eval{\formula}_{\{x\mapsto n\}}=\vtrue$} \\
          \bot  & \text{otherwise}
        \end{cases}
      \right\}\\
    \MinVal{\rtys\to\rty} &=
      \left\{ v \mapsto
        \begin{cases}
          \MinVal{\rty} & \text{if $v \sqsupseteq \MinVal{\rtys}$} \\
          \bot          & \text{otherwise}
        \end{cases}
      \right\}\\
    \MinVal{\rty_1\wedge...\wedge\rty_k} &=
      \MinVal{\rty_1}\sqcup...\sqcup\MinVal{\rty_k}
  \end{align*}
  The set of valuations $\MinVal{\RefTyEnv;\formula}$ is defined by
  \begin{align*}
    \MinVal{\RefTyEnv;\formula} =
      \{ \rho \mid
            (\forall x:\rty\in\RefTyEnv.
              \rho(x)\sqsupseteq\MinVal{\rty_\rho})
          \text{ and }
            \eval{\formula}_\rho=\vtrue
      \}
  \end{align*}
  Therein, $\rty_\rho$ is the refinement type obtained by replacing all free variables
  $x$ in $\rty$ with $\rho(x)$.
\end{Definition}%}}}
\begin{Lemma}\label{lem:soundness_of_ref}%{{{
  If $\RefTyEnv\ctxSep\formula \vdash \psi : \rty$ and
  $\rho \in \MinVal{\RefTyEnv;\formula}$, then
  $\eval{\psi}_\rho \sqsupseteq \MinVal{\rty_\rho}$.
\end{Lemma}%}}}
\begin{proof*}%{{{
  Induction on the derivation with case analysis on the last rule used.
  \begin{itemize}
    \item \inflabel{R-Var}, \inflabel{R-Pred}. %{{{
      By definition of $\MinVal{\RefTyEnv;\formula}$.
      %}}}
    \item \inflabel{R-And}, \inflabel{R-Or}. %{{{
      By simple application of induction hypothesis.
      %}}}
    \item \inflabel{R-App}. %{{{
      Let $\rho \in \MinVal{\RefTyEnv;\formula}$. By induction hypothesis,
      \begin{align*}
        \eval{\psi_1}_\rho &\sqsupseteq \MinVal{\rty_{1_\rho}\wedge...\wedge\rty_{k_\rho}\to\rty_\rho} \\
        \eval{\psi_2}_\rho &\sqsupseteq \MinVal{\rty_{i_\rho}} \text{ for each $i$}
      \end{align*}
      We have
        $\eval{\psi_1}_\rho \eval{\psi_2}_\rho \sqsupseteq \MinVal{\rty_\rho}$
      since
        $\eval{\psi_2}_\rho \sqsupseteq \MinVal{\rty_{1_\rho}\wedge...\wedge\rty_{k_\rho}}$.
      %}}}
    \item \inflabel{R-Abs}. %{{{
      \infrule[R-Abs]
        {\RefTyEnv,x:\rty_1,...,x:\rty_k \ctxSep \formula
          \vdash \psi : \rty}
        {\RefTyEnv \ctxSep \formula
          \vdash \lambda x. \psi : \rty_1\wedge...\wedge\rty_k\to\rty}
      Let $\rho \in \MinVal{\RefTyEnv;\formula}$ and
          $v$ be a value with $v \sqsupseteq \MinVal{\rty_1\wedge...\wedge\rty_k}$.
      We have
      \begin{align*}
        \eval{\lambda x. \psi}_\rho v
        =           \eval{\psi}_{\rho\cup\{x\mapsto v\}}
        \sqsupseteq \MinVal{\rty_\rho}
      \end{align*}
      by induction hypothesis. Thus, we have
        $\eval{\lambda x. \psi}_\rho \sqsupseteq \MinVal{\rty_1\wedge...\wedge\rty_k\to\rty}$.
      %}}}
    \item \inflabel{R-IntApp}. %{{{
      Let $\rho \in \MinVal{\RefTyEnv;\formula\land[a/x]\formula'}
         (\subseteq \MinVal{\RefTyEnv;\formula})$ and
         $n = \eval{a}_\rho$.
      By induction hypothesis, we have
      $\eval{\psi}_\rho \sqsupseteq \MinVal{x:\formula'_\rho\to\rty_\rho}$.
      Since
        $\eval{[a/x]\formula'}_\rho
        = \eval{\formula'}_{\rho\cup\{x\mapsto n\}}
        = \vtrue$,
      $\eval{\psi a}_\rho
        =           \eval{\psi}_\rho n
        \sqsupseteq \MinVal{\rty_{\rho\cup\{x\mapsto n\}} }
        =           \MinVal{[a/x]\rty_\rho}$.
      %}}}
    \item \inflabel{R-IntAbs}. %{{{
      Let $\rho \in \MinVal{\RefTyEnv;\formula}$ and
          $n$ be an integer with $\eval{\formula'}_{\rho \cup \{x\mapsto n\}}=\vtrue$.
      By induction, we have
      \begin{align*}
        \eval{\lambda x. \psi}_\rho n
        = \eval{\psi}_{\rho\cup\{x\mapsto n\}}
            \sqsupseteq \MinVal{\rty_{\rho\cup\{x\mapsto n\}} }
      \end{align*}
      %}}}
    \item \inflabel{R-Nu}. %{{{
      By induction hypothesis (and the same discussion as \inflabel{R-Abs}),
      $\eval{\lambda x. \psi}_\rho \sqsupseteq \MinVal{\rty_\rho\to\rty_\rho}$ holds
      for any $\rho \in \MinVal{\RefTyEnv;\formula}$. Let $f = \eval{\lambda x. \psi}_\rho$.
      The goal is to show $\mathrm{gfp}(f) \sqsupseteq \MinVal{\rty_\rho}$. In other word,
      to show $f^\alpha(\top) \sqsupseteq \MinVal{\rty_\rho}$ for any cardinal $\alpha$.
      Therein, $f^\alpha(x)$ is defined by
      \begin{align*}
        f^0(x) &= x \\
        f^{(n+1)}(x) &= f(f^n(x)) \\
        f^\gamma(x) &= \bigsqcap\{f^\alpha(x) \mid \alpha < \gamma\} \text{ if $\gamma$ is a limit cardinal}
      \end{align*}
      For $\alpha = n$, it can be shown by trivial induction. For a limit cardinal $\gamma$,
        $f^\gamma(\top) = \bigsqcap\{f^\alpha(\top) \mid \alpha < \gamma\}
              \sqsupseteq \bigsqcap\{\MinVal{\rty_\rho} \mid \alpha < \gamma\}
                        = \MinVal{\rty_\rho}$
      by induction hypothesis.
      %}}}
    \item \inflabel{R-Coerce}. %{{{
      By the following Lemma~\ref{lem:soundness_of_ref:subtype}. \QED
      %}}}
  \end{itemize}
\end{proof*}%}}}
\begin{Lemma}\label{lem:soundness_of_ref:subtype}%{{{
  If $\RefTyEnv\ctxSep\formula\vdash\rty\subType\rty'$ and $\rho\in\MinVal{\RefTyEnv;\formula}$,
  then $\rty_\rho \sqsupseteq \rty'_\rho$.
\end{Lemma}%}}}
\begin{proof*}%{{{
  Induction on the derivation with the case analysis on the last rule used.
  Since cases of \inflabel{RSub-Base} and \inflabel{RSub-Arrow} are easy, we show only
  case of \inflabel{RSub-IntArrow}.
  \infrule[RSub-IntArrow]
    {\models \formula\land\formula_x'\implies\formula_x \andalso
     \RefTyEnv,x:\tInt \ctxSep \formula\land\formula_x'
      \vdash \rty \preceq \rty'}
    {\RefTyEnv \ctxSep \formula
      \vdash (x:\formula_x\to\rty) \preceq (x:\formula'_x\to\rty')}
  Let $\rho \in \MinVal{\RefTyEnv;\formula}$ and
      $n$ be an integer with $\eval{\formula_x'}_{\rho\cup\{x\mapsto n\}} = \vtrue$.
  Since $\rho' = {\rho\cup\{x\mapsto n\}}
             \in \MinVal{\RefTyEnv;x:\tInt,\formula\land\formula_x'}$,
  we have
    $\MinVal{(x:\formula_x\to\rty)_\rho} n
      = \MinVal{\rty_{\rho'}}
      \sqsupseteq \MinVal{\rty'_{\rho'}}
      = \MinVal{\rty'_{\rho\cup\{x\mapsto n\}} }$
  by induction (Note that $n$ also satisfies $\eval{\formula_x}_{\rho\cup\{x\mapsto n\}}$).
  Thus, $\MinVal{(x:\formula_x \to\rty )_\rho} \sqsupseteq
         \MinVal{(x:\formula_x'\to\rty')_\rho}$ holds.
\end{proof*}%}}}
\begin{Theorem}[Soundness of refinement type system]%{{{
  For \nuHFLz formula $D_\psi$ written in HES
    and refinement type environment $\RefTyEnv$,
  if $D_\psi : \RefTyEnv$ then $D_\psi$ is valid.
\end{Theorem}%}}}
\begin{proof*}%{{{
  Let $\{F_0 =_\nu \psi_0;...;F_n=_\nu \psi_n\} = D_\psi$,
      $\rtys_i = \RefTyEnv(F_i)$.
  By Lemma~\ref{lem:soundness_of_ref},
    $\eval{\psi_i}_\rho \sqsupseteq \MinVal{\rty_\rho}$ holds for any
    $\rho \in \MinVal{\AbstTyEnv;\true}$.
  Let $D_\varphi = \{F_0 =_\nu \varphi_0;...;F_n =_\nu \varphi_n\}$.
  As described in the Chapter~\ref{chapter:Preliminaries}, it is known that
  $\eval{D_\psi}$ is equal to $\rho^*(F_0)$ where
  $\rho^*$ is the greatest fixpoint of the following function $f$:
  \begin{align*}
    f(\rho) = \{ X \mapsto \eval{\psi}_\rho \mid X =_\nu \psi \in D_\psi\}
  \end{align*}
  So, it is sufficient to show
    $\rho^* \in \MinVal{\AbstTyEnv;\true}$.
  It is shown by the same discussion as
  the case of \inflabel{R-Nu} of the proof of Lemma~\ref{lem:soundness_of_ref}.
\end{proof*}%}}}
%}}} End of \chapter{Soundness of refinement type system}
