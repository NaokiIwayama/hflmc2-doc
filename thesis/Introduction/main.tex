\chapter{Introduction}\label{chapter:Introduction}
\par
  \HFLz is a logic that includes higher-order functions, modal operators, fixpoint operators, and integers.
  % \TODO{model checkingとvalidity checkingの説明? model checkingには全く触れないで良い？}.
  Recently, it has been shown~\cite{KobayashiESOP18,WatanabePEPM19} that various kinds of verification problems of functional programs
  can be naturally translated into (modal-free) \HFLz validity checking problems.
\par
  Following this result, Suzuki~\cite{Suzuki} developed a method of automated model checking of \nuHFLz,
  a sub-logic of \HFLz without least fixpoint operators.
  Their method is based on a framework called \emph{predicate abstraction}~\cite{PredicateAbstraction} and
  \emph{counterexample-guided abstraction refinement (CEGAR)}~\cite{CEGAR1,CEGAR2} in which
  \nuHFLz formulas are repeatedly abstracted into \emph{pure} \nuHFL formula, which does not contain integers.
  The overall structure is shown in Figure \ref{pic:CEGAR}.
  First, they abstract \nuHFLz formula into pure \nuHFL formula using information called \emph{predicate} (Step 1).
  As pure ($\nu$-)\HFL validity checking is decidable, the validity of pure \nuHFLz formula can be determined by
  existing \HFL validity checkers (Step 2).
  If it is valid, so is the original \nuHFLz formula since abstraction is sound.
  If not, the pure validity checkers give us a \emph{counterexample} or a certificate of invalidity.
  They then check if the counterexample is \emph{feasible} in the original \nuHFLz formula, or
  it has enough information to disprove the validity of the original formula.
  If it is infeasible, they then refine the predicates using the counterexample (Step 4) and proceed to the next loop.
  % Following this procedure, they achieved fully-automated \nuHFLz validity checking.
\par
  Their method, however, has a problem with its verification power.
  Let us see the following program with assertions.
  \begin{align*}
    % &\mathrm{let}\;\mathrm{forall}\;p =_\nu p\;(\mathrm{rand\_int()}) \;\mathrm{in}\\
    % TODO rand_intがforallの意味になるのはmay-reachabilityのときだけ
    &\mathrm{let}\;\mathrm{nonNegative}\;n = n \ge 0 \;\mathrm{in}\\
    &\mathrm{let}\;\mathrm{nonPositive}\;n = n \le 0 \;\mathrm{in}\\
    &\mathrm{let}\;x = \mathrm{rand\_int()} \;\mathrm{in}\\
    &\mathrm{assert}(\mathrm{nonNegative}\;x \lor \mathrm{nonPositive}\;x)
  \end{align*}
  This program asserts that ``for any randomly taken integer $x$, $x$ is either non-negative or non-positive'' and
  existing program verification tools such as MoCHi~\cite{MoCHi,MuraseFairTerm,KuwaharaNonterm,WatanabeFairNonTerm}
  can verify that this assertion does not fail for any $x$.
  Using the reduction of Kobayashi et al.~\cite{KobayashiESOP18} and Watanabe et al.~\cite{WatanabePEPM19},
  this program verification problem is translated into the validity checking problem of following \nuHFLz formula,
  which cannot be proven to be valid by Suzuki's tool.
  \begin{align*}
    &\mathrm{let}\;\mathrm{Forall}\;n\;p =_\nu p\;n \land \mathrm{Forall}\;(n-1)\;p \land \mathrm{Forall}\;(n+1)\;p\;\mathrm{in}\\
    &\mathrm{let}\;\mathrm{NonNegative}\;n =_\nu n \ge 0 \;\mathrm{in}\\
    &\mathrm{let}\;\mathrm{NonPositive}\;n =_\nu n \le 0 \;\mathrm{in}\\
    &\mathrm{Forall}\;0 \;(\lambda x. \mathrm{NonNegative}\;x \lor \mathrm{NonPositive}\;x)
  \end{align*}
  The problem lies in its abstraction on integers.
  It abstract integer expressions into tuples of booleans which semantically correspond to conjunctions of predicates.
  In the above formula,
  given predicates $x \ge 0$ and $x \le 0$, $x$ is abstracted into $(\false,\false)$ which means that
  both $x \le 0$ and $x \ge 0$ are not always true.
  Thus, $(\mathrm{NonNegative}\;x \lor \mathrm{NonPositive}\;x)$ is abstracted into $(\false \lor \false)$ and
  the whole formula is not proven to be valid.
  To remedy this problem, they proposed to use another kind of predicates called branching conditions.
  However, the inference of good branching conditions is difficult and
    they introduced a restriction on the use of it in their implementation, under which
    the above example cannot be proved after all.
\par
  To overcome this problem, we developed a new method of predicate abstraction,
  in which integers are abstracted into a set of tuples of booleans that semantically corresponds to
  disjunctions of conjunctions of predicates.
  In the example above, the integer $x$ is abstracted into
    $\{(\true,\false),(\false,\true),(\true,\true)\}$ which means that
  $x$ satisfies $x \ge 0$, $x \le 0$, \emph{or} both $x \ge 0$ and $x \le 0$.
  With disjunctions, we can now say that either $x \ge 0$ or $x \le 0$ always holds and
  hence prove the validity of the whole formula.
\par
  Theoretically, we proved
    \emph{soundness} and
    \emph{relative completeness respect to the refinement intersection type system} of our method.
  The former is a property that the result of abstraction is an underapproximation of the original formula
    and thus it ensures the correctness of our whole algorithm.
  The latter guarantee that
    our predicate abstraction has at least the same verification power as the refinement type system.
  In other words, if a formula is proved to be valid by typing refinement type, we can also prove the validity by our method.
  Although it is desirable for the CEGAR algorithm to have a property called \emph{progress} which means that
    the CEGAR loop does not get stuck,
    the proof of this property is left as a conjecture.
\par
  In practice, we implemented a validity checker based on the proposed method.
  We also conducted experiments and confirmed that our tool can solve more instances
  than Suzuki's tool though in performance it is not good as Suzuki's.
\par
  The rest of this thesis is structured as follows.
  Section 2 defines the \nuHFLz and \nuHFL, the source and target logic of predicate abstraction.
  Section 3 defines our predicate abstraction algorithm and proves its correctness.
  It is also proven that our abstraction system is relatively complete with respect to
  the refinement type system for \nuHFLz.
  Section 4 defines the counterexample-guided abstraction refinement procedure for our predicate abstraction system.
  Section 5 discusses our implementation and experiments.
  Section 6 discusses related work, and Section 7 concludes this thesis.

\begin{figure}
  \centering
    \includegraphics[width=0.8\textwidth]{./Introduction/CEGAR.eps}
  \caption{\nuHFLz validity checking based on predicate abstraction and CEGAR}
  \label{pic:CEGAR}
\end{figure}

