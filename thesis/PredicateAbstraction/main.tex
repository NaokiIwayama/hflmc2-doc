\chapter{Predicate Abstraction}
  This chapter defines our predicate abstraction procedure, the main part of this thesis.
  It translates \nuHFLz formula into \nuHFL formula that underapproximates the input formula.
  Like Suzuki, our method is based on the type-directed translation proposed by Kobayashi et al.~\cite{MoCHi}.
\section{Definition of Our Predicate Abstraction} %{{{
  Following Kobayashi et al.~\cite{MoCHi} and Suzuki, we use
  \emph{abstraction type} as an interface of predicate abstraction.
  The syntax of the abstraction type is defined as follows.
  \begin{Definition}[Abstraction type]
    \begin{alignat*}{3}
      &(\text{abstraction type})\quad&
        \aty &::= \tBool \mid x:\tInt[P_1,...,P_k]\to\aty \mid \aty_1\to\aty_2 \\
      &(\text{extended abstraction type})\quad&
        \ex{\aty} &::= \aty \mid \tInt \\
      &(\text{predicate})\quad&
        P,Q &::= \true \mid \false
                 \mid \pred(\tilde{a})
                 \mid P_1 \land P_2
                 \mid P_1 \lor  P_2 \\
    &(\text{environment})\quad&
      \AbstTyEnv &::= \emptyset
                 \mid \AbstTyEnv, x:\ex{\aty}
    \end{alignat*}
  \end{Definition}
  We use the metavariable $\predSet$ for a sequence of predicates $P_1,...,P_k$.
\par
  Intuitively, the abstraction type tells us how we should abstract formulas.
  For example, if $x:\tInt[x=0]\to\tBool$ is given as an abstraction type for formula $\lambda x^\tInt.\psi$,
  we abstract $\psi$ by case analysis on whether $x=0$ holds or not.
\par
  Like standard refinement type system,
  we do not consider types that violate variables' scopes such as
    $x:\tInt[x=y] \to y:\tInt[] \to \tBool$.
  We also require that for each $x:\aty \in \AbstTyEnv$,
    i.e., $\AbstTyEnv = \AbstTyEnv',x:\aty,\AbstTyEnv''$ for some $\AbstTyEnv'$ and $\AbstTyEnv''$,
  $\aty$ is closed under $\AbstTyEnv'$.
  For example, $(x: \tInt, f : (y:\tInt[x=y]\to\tBool))$ is a well-formed environment but
               $(f : (y:\tInt[x=y]\to\tBool), x: \tInt)$ is not.
\par
  Following Suzuki~\cite{Suzuki}, we define two functions $(\_)^\sharp$ and $(\_)^\flat$ which translate
  abstraction types into simple types, before and after abstraction respectively.
  \begin{gather*}
    \tBool^\sharp = \tBool \andalso
    (\tInt[P_1,...,P_k]\to\aty)^\sharp = \tInt\to\aty^\sharp \andalso
    (\aty_1\to\aty_2)^\sharp = \aty_1^\sharp\to\aty^\sharp \\
    \tBool^\flat = \tBool \andalso
    (\tInt[P_1,...,P_k]\to\aty)^\flat = \overbrace{\tBool\to\cdots\to\tBool}^{k}\to\aty^\flat \andalso
    (\aty_1\to\aty_2)^\flat = \aty_1^\flat\to\aty^\flat
  \end{gather*}
  These functions are naturally extended to abstraction type environments:
  \begin{gather*}
    \emptyset^\sharp = \emptyset \andalso
    (\AbstTyEnv,x:\tInt)^\sharp = \AbstTyEnv^\sharp, x:\tInt \andalso
    (\AbstTyEnv,x:\aty)^\sharp = \AbstTyEnv^\sharp, x:\aty^\sharp \\
    \emptyset^\flat = \emptyset \andalso
    (\AbstTyEnv,x:\tInt)^\flat = \AbstTyEnv^\flat \andalso
    (\AbstTyEnv,x:\aty)^\flat = \AbstTyEnv^\flat, x:\aty^\flat
  \end{gather*}
  Notably, $(\_)^\flat$ is also extended to $\predSet$:
  \begin{gather*}
    (P_1,...,P_k)^\flat = b_{P_1}:\tBool, ..., b_{P_k} : \tBool
  \end{gather*}
  Here, $b_P$ is a preserved variable which corresponds to the truth value of predicate $P$ after abstraction.
  We write $(\AbstTyEnv\ctxSep\predSet)^\flat$ to denote $\AbstTyEnv^\flat \cup \predSet^\flat$.
\par
  Our predicate abstraction is defined by the relation
    $\AbstTyEnv \ctxSep \predSet \vdash \psi : \sigma \absInto \varphi$
  in Figure~\ref{rules:abst}.
  Intuitively, the relation means that
    if $\psi$ is \nuHFLz formula with $\AbstTyEnv^\sharp \vdash_\mathrm{ST} \psi : \sigma^\sharp$
    then $\varphi$ is \nuHFL formula with $(\AbstTyEnv\ctxSep\predSet)^\flat \vdash_\mathrm{ST} \varphi : \sigma^\flat$
    which is an underapproximation of $\psi$ obtained by focusing the predicates in
    $\AbstTyEnv, \predSet$, and $\sigma$.
  For HES, abstraction relation $\vdash D_\psi : \AbstTyEnv \absInto D_\varphi$ is also defined in
  Figure~\ref{rules:abst}.
\par
  We explain the main rules.
  \inflabel{A-Pred} translate a predicate into a corresponding boolean variable
    if the predicate is currently available in $\predSet$.
  In \inflabel{A-IntAbs}, the bound variable $x$ of $\lambda x.\psi$ is translated into
    boolean variables $b_{P_1},...,b_{P_k}$ where $P_1,...,P_k$ is
    predicates attached to $x$. In the abstraction of body $\psi$, $P_1,...,P_k$ gets available.
  Conversely in \inflabel{A-IntApp}, arithmetic application $\psi a$ requires
    $[a/x]P_1,...,[a/x]P_k$ to be available in the context.
  The \inflabel{A-Coerce} rule defines coercion of both predicates in context ($\predSet$) and
  abstraction types ($\aty$) using the coercion relation described later.
\par
  The coercion relation
    $\AbstTyEnv \vdash \varphi : (\predSet, \aty) \subType (\predSet',\aty') \absInto \varphi'$
    means that if $\varphi$ is \nuHFL formula obtained by abstraction using
    $\AbstTyEnv, \predSet$, and $\aty$, then $\varphi'$ is a \nuHFL formula
    obtained by abstraction using $\AbstTyEnv, \predSet'$, and $\aty'$.
  The most important rule $\inflabel{AC-Base}$ defines the coercion of the base type $\tBool$.
  The rule can be understood as follows. The variable $X$ represents a $k$-holed context and
  $\xi$ is a $l$-holed context that can depend on $X$.
  $\eval{X P_1...P_k}_\rho \sqsupseteq_{\tBool} \eval{\xi Q_1...Q_l}_\rho$ means that
  if the holes are filled with $\tilde{P}$ and $\tilde{Q}$ respectively, then
  $\xi \tilde{Q}$ is an underapproximation of $X \tilde{P}$.
  Given $\tilde{P} \equiv x=0$ and $\tilde{Q} \equiv x\le0,x\ge0$, for example,
  $\xi \equiv \lambda b_1.\lambda b_2. X (b_1 \land b_2)$ satisfies the condition
  because $\xi (x \le 0) (x \ge 0) = X (x \le 0 \land x \ge 0) = X (x=0)$.
  If $\tilde{P}$ and $\tilde{Q}$ are flipped, $\xi = \lambda b. X b b$ does because
  $\xi (x = 0) = X (x=0) (x=0) \implies X (x\le0) (x\ge0)$. The last implication holds because of
  the monotonicity of $X$ (Remember that every \nuHFL formula is monotonic).
  Another interesting example is $\tilde{P}=x\le0, x\ge0$, $\tilde{Q}=\varepsilon$ and
  $\xi = X\;\true\;\false \land X\;\false\;\true \land X\;\true\;\true$.
  This corresponds to the fact that one of
    $x \ge 0$, $x \le 0$, and $x \ge 0 \land x \le 0$ always holds for any integer $x$.
  Therefore, it can be said that our predicate abstraction translate an integer $x$ into
  a set of tuples of booleans $\{ (b_{11},...,b_{1k}),...,(b_{m1},...,b_{mk}) \}$
  which satisfies
    $\models
     \bigvee_{i \in \{1,...,m\}}
     \bigwedge_{j\in\{1,...,k\}} (b_{ij}\iff P_{j})$
    \footnote{
      To be precise, $b_{ij} \implies P_{j}$ is enough because of monotonicity:
      in the above example, $(\true,\true)$ is redundant since
      $(X\;\true\;\false)$ implies $(X\;\true\;\true)$.
    }.
  Note that \inflabel{AC-Base} is non-deterministic in the choice of $\xi$.
  In general, the best $\xi$ is given by the following formula.
  \begin{align*}
    \xi = \lambda b_{Q_1}....\lambda b_{Q_l}.\;
      \bigvee_{(I, \{J_\lambda\}_{\lambda \in \Lambda}) \in \mathcal{C}} \left(
        \bigwedge_{i \in I} b_{Q_i} \land
        \left(
          \bigwedge_{J \in \{J_\lambda\}_{\lambda \in \Lambda}}
          X\;p_{1,J} \dots p_{n,J}
        \right)
      \right)\\
  \end{align*}
  where
  \begin{align*}
    \mathcal{C} &= \left\{
      (I, \{J_\lambda\}_{\lambda \in \Lambda}) \;\middle|\;
      \models \bigwedge_{i \in I} Q_i
        \implies \bigvee_{\lambda \in \Lambda} \bigwedge_{j \in J_\lambda} P_j
    \right\} \\
    p_{j,J} &= \begin{cases}
        \true  & \text{ if } j \in J \\
        \false & \text{ otherwise }
    \end{cases}
  \end{align*}
  This is a generalization of the discussion above based on the case analysis on
  which subset of $\{Q_1,...,Q_l\}$ holds
  ($\{J_\lambda\}_{\lambda \in \Lambda}$ corresponds to
   $\{ (b_{11},...,b_{1k}),...,(b_{m1},...,b_{mk}) \}$).
\begin{figure}[ht!] %{{{ Abstraction relation
  \newcommand{\tmpskip}{8pt}
  \infrule[A-Var]
    {}
    {\AbstTyEnv, x:\aty \ctxSep \predSet
        \vdash x:\aty
        \absInto x}
    \vspace{\tmpskip}
  \infrule[A-Pred]
    {p(\tilde{a}) \in \predSet}
    {\AbstTyEnv \ctxSep \predSet
        \vdash \pred(\tilde{a}): \tBool
        \absInto b_{p(\tilde{a})}}
    \vspace{\tmpskip}
  \infrule[A-IntApp]
    {\AbstTyEnv \ctxSep \predSet
        \vdash \psi : (x:\tInt[P_1,...,P_k]\to\aty)
        \absInto \varphi \andalso
     \AbstTyEnv \vdash_{\mathrm{ST}} a: \tInt}
    {\AbstTyEnv \ctxSep \predSet, [a/x]\tilde{P}
        \vdash \psi a : [a/x]\aty
        \absInto \varphi \tilde{b}_{[a/x]P}}
    \vspace{\tmpskip}
  \infrule[A-IntAbs]
    {\AbstTyEnv,x:\tInt \ctxSep \predSet,\tilde{P}
        \vdash \psi : \aty
        \absInto \varphi}
    {\AbstTyEnv \ctxSep \predSet
        \vdash \lambda x. \psi : (x:\tInt[\tilde{P}]\to\aty)
        \absInto \lambda \tilde{b}_P. \varphi}
    \vspace{\tmpskip}
  \infrule[A-App]
    {\AbstTyEnv \ctxSep \predSet
        \vdash \psi_1 : \aty_1\to\aty_2
        \absInto \varphi_1 \andalso
      \AbstTyEnv \ctxSep \predSet
        \vdash \psi_2 : \aty_1
        \absInto \varphi_2}
    {\AbstTyEnv \ctxSep \predSet
        \vdash \psi_1\psi_2 : \aty_2
        \absInto \varphi_1\varphi_2}
    \vspace{\tmpskip}
  \infrule[A-Abs]
    {\AbstTyEnv,x:\aty_1 \ctxSep \predSet
        \vdash \psi : \aty_2
        \absInto \varphi}
    {\AbstTyEnv \ctxSep \predSet
        \vdash \lambda x. \psi : \aty_1\to\aty_2
        \absInto \lambda x. \varphi}
    \vspace{\tmpskip}
  \infrule[A-And]
    {\AbstTyEnv \ctxSep \predSet
        \vdash \psi_1 : \tBool
        \absInto \varphi_1 \andalso
     \AbstTyEnv \ctxSep \predSet
        \vdash \psi_2 : \tBool
        \absInto \varphi_2}
    {\AbstTyEnv \ctxSep \predSet
        \vdash \psi_1\land\psi_2
        \absInto \varphi_1\land\varphi_2}
    \vspace{\tmpskip}
  \infrule[A-Or]
    {\AbstTyEnv \ctxSep \predSet
        \vdash \psi_1 : \tBool
        \absInto \varphi_1 \andalso
     \AbstTyEnv \ctxSep \predSet
        \vdash \psi_2 : \tBool
        \absInto \varphi_2}
    {\AbstTyEnv \ctxSep \predSet
        \vdash \psi_1\lor\psi_2
        \absInto \varphi_1\lor\varphi_2}
    \vspace{\tmpskip}
  \infrule[A-Nu]
    {\AbstTyEnv,x:\aty \ctxSep \predSet
        \vdash \psi : \aty
        \absInto \varphi}
    {\AbstTyEnv \ctxSep \predSet
        \vdash \nu x.\psi : \aty
        \absInto \nu x. \varphi}
    \vspace{\tmpskip}
  \infrule[A-Coerce]
    {\AbstTyEnv \ctxSep \predSet
        \vdash \psi : \aty
        \absInto \varphi \andalso
     \AbstTyEnv \vdash \varphi
        :         (\predSet, \aty )
        \subType  (\predSet',\aty')
        \absInto \varphi'}
    {\AbstTyEnv \ctxSep \predSet'
        \vdash \psi : \aty'
        \absInto \varphi'}
    \vspace{\tmpskip}
  \infrule[AC-Base]
    {X:\tBool^k\to\tBool \vdash \xi : \tBool^l\to\tBool \\
     \eval{X   P_1...P_k}_\rho \sqsupseteq_{\tBool} \eval{\xi Q_1...Q_l}_\rho
        \text{ for all } \rho
        \text{ s.t. } \dom{\rho} = \{X\}\cup\{x \mid x:\tInt \in \AbstTyEnv \}}
    {\AbstTyEnv \vdash \varphi
        :        (P_1,...,P_k, \tBool)
        \subType (Q_1,...,Q_l, \tBool)
        \absInto [\lambda b_{P_1}...b_{P_k}.\varphi/X]\xi b_{Q_1}...b_{Q_l}}
    \vspace{\tmpskip}
  \infrule[AC-IntArrow]
    {\AbstTyEnv,x:\tInt \vdash \varphi b_{P_1}...b_{P_k}
        :        (\predSet_1\cup\{P_1,...,P_k\}, \aty_1)
        \subType (\predSet_2\cup\{Q_1,...,Q_l\}, \aty_2)
        \absInto \varphi'}
    {\AbstTyEnv \vdash \varphi
        :        (\predSet_1, x:\tInt[P_1,...,P_k]\to\aty_1)
        \subType (\predSet_2, x:\tInt[Q_1,...,Q_l]\to\aty_2)
        \absInto \lambda b_{Q_1}\cdots b_{Q_l}. \varphi'}
    \vspace{\tmpskip}
  \infrule[AC-Arrow]
    {\AbstTyEnv,x:\aty_1' \vdash x
        :        (\varepsilon, \aty_1')
        \subType (\predSet', \aty_1 )
        \absInto \varphi_1 \andalso
     \AbstTyEnv,y:\aty_1 \vdash \varphi y
        :        (\predSet , \aty_2)
        \subType (\predSet', \aty_2')
        \absInto \varphi_2'}
    {\AbstTyEnv \vdash \varphi
        :        (\predSet , \aty_1 \to\aty_2 )
        \subType (\predSet', \aty_1'\to\aty_2')
        \absInto \lambda x. [\varphi_1/y]\varphi_2'}
    \vspace{\tmpskip}
  \infrule[AHes]
    {\AbstTyEnv = F_0 : \aty_0, ..., F_n : \aty_n \andalso
     \AbstTyEnv \ctxSep \varepsilon \vdash
        \psi_i : \aty_i \absInto \varphi_i \text{ for each $i \in \{0,...,n\}$}}
    {\vdash   (F_0 =_\nu \psi_0   ; ...; F_n =_\nu \psi_n   ) : \AbstTyEnv
     \absInto (F_0 =_\nu \varphi_0; ...; F_n =_\nu \varphi_n) }
\caption{Abstraction relation}
\label{rules:abst}
\end{figure} %}}} End of Abstraction relation

  \begin{Example}\label{example:3:PA_HES}
    Given $\AbstTyEnv = \{
      \mathrm{S} : \tBool,
      \mathrm{Forall} : n:\tInt[]\to(x:\tInt[x\ge0,x\le0]\to\tBool)\to\tBool,
      \mathrm{NonNegative} : n:\tInt[n \ge 0] \to \tBool,
      \mathrm{NonPositive} : n:\tInt[n \le 0] \to \tBool
    \}$,
    Example~\ref{example:2:HES} can be abstracted into the following \nuHFL.
    \begin{align*}
      \mathrm{S} &=_\nu \mathrm{Forall}\;
        (\lambda b_{x\ge0}.\lambda b_{x\le0}.
          \mathrm{NonNegative}\;b_{x\ge0} \lor
          \mathrm{NonPositive}\;b_{x\le0}); \\
      \mathrm{Forall} &=_\nu \lambda p.\;
        (p\;\true \;\false \land
         p\;\false\;\true  \land
         p\;\true \;\true) \\
        &\phantom{\quad\quad\quad}\land
        \mathrm{Forall}\;p \land
        \mathrm{Forall}\;p;\\
      \mathrm{NonNegative} &=_\nu \lambda b_{n\ge0}.\;b_{n\ge0};\\
      \mathrm{NonPositive} &=_\nu \lambda b_{n\le0}.\;b_{n\le0}
    \end{align*}
    This \nuHFL is valid. Thus, the \nuHFLz formula defined in Example~\ref{example:2:HES} is also valid.
  \end{Example}

\subsection{Difference between Suzuki's predicate abstraction} %{{{
  This section discusses the difference between our predicate abstraction and Suzuki's.
  Suzuki's predicate abstraction can be considered as a restriction of ours
    except \emph{branching condition} (explained later).
  For example, their integer-application rule
  \begin{gather*}
    \infer
      {\AbstTyEnv \ctxSep \predSet
          \vdash \psi a : [a/x]\aty
          \absInto \varphi \tilde{\varphi}}
      {\AbstTyEnv \ctxSep \predSet
          \vdash \psi : (x:\tInt[P_1,...,P_k]\to\aty)
          \absInto \varphi \andalso
       \AbstTyEnv \vdash_{\mathrm{ST}} a: \tInt \andalso
       \varphi_i = \mathrm{WPC}_\predSet(P_i)
       }\\
    \mathrm{WPC}_{\tilde{Q}}(P) = \xi \tilde{b}_{Q}
       \quad \text{(if $\xi \tilde{Q}$ is the weakest precondition of $P$ using $\tilde{Q}$)}
  \end{gather*}
  can be regarded as combination of our \inflabel{A-IntApp} and
  (restricted) \inflabel{AC-Base} as follows.
  \begin{align*}
    \infer
      {\AbstTyEnv \ctxSep \predSet
          \vdash \psi a : [a/x]\aty
          \absInto \varphi \tilde{\varphi}}
      {\infer
        {\AbstTyEnv \ctxSep \predSet, [a/x]\tilde{P}
          \vdash \psi a : [a/x]\aty
          \absInto \varphi \tilde{b}_{[a/x]P_i}}
        {\AbstTyEnv \ctxSep \predSet
            \vdash \psi : (x:\tInt[\tilde{P}]\to\aty)
            \absInto \varphi}
      \andalso
      \infer
        {\begin{minipage}{18em}
          $\AbstTyEnv \ctxSep \predSet, [a/x]\tilde{P}
            \vdash \varphi \tilde{b}_{[a/x]P_i} : $ \\
          \quad\quad
         $((\predSet,\tilde{P}), [a/x]\aty)
            \subType (\predSet, [a/x]\aty)
            \absInto \varphi \tilde{\varphi}$
        \end{minipage} }
        {\eval{X \tilde{Q} \tilde{P}}_\rho \sqsupseteq_{\tBool} \eval{\xi \tilde{P}}_\rho 
         \text{ where }
         \xi = \lambda \tilde{b}_Q.\;X \tilde{b}_Q\tilde{\varphi},\;
         \predSet = \tilde{Q}}
      }
  \end{align*}
\par
  In their rule, each predicate $P_i$ is abstracted independently by
    $\varphi_i = \mathrm{WPC}_\predSet(P_i)$, and therefore
  it cannot make use of information on disjunction of predicates like
    ``$x \le 0$ \emph{or} $x \ge 0$ always holds''.
  As a result, it translate Example~\ref{example:2:HES} into
  invalid \nuHFL under the same abstraction environment:
    \begin{align*}
      \mathrm{S} &=_\nu \mathrm{Forall}\;
        (\lambda b_{x\ge0}.\lambda b_{x\le0}.
          \mathrm{NonNegative}\;b_{x\ge0} \lor
          \mathrm{NonPositive}\;b_{x\le0}); \\
      \mathrm{Forall} &=_\nu \lambda p.\
        \boldsymbol{p} \;\mathbf{false}\;\mathbf{false} \land \mathrm{Forall}\;p \land \mathrm{Forall}\;p;\\
      \mathrm{NonNegative} &=_\nu \lambda b_{n\ge0}.\;b_{n\ge0};\\
      \mathrm{NonPositive} &=_\nu \lambda b_{n\le0}.\;b_{n\le0}
    \end{align*}
\par
  To overcome this problem, Suzuki introduced another kind of predicates called branching conditions.
  The branching condition $\formula_b$ is used to underapproximate formula
    $\psi_1 \lor \psi_2$ by $(\formula_b\implies\psi_1) \land (\neg\formula_b\implies\psi_2)$.
  For example, given $\formula_b = x \ge 0$,
    $\mathrm{NonPositive}\;x\lor\mathrm{NonPositive}\;x$ is underapproximate by
    $(x \ge 0 \implies \mathrm{NonPositive}\;x)\land
     (x <   0 \implies \mathrm{NonPositive}\;x)$.
  Then, they can abstract this into valid \nuHFL formula
    $(\mathrm{NonPositive}\;\true\land\mathrm{NonNegative}\;\true)$.
  However, inference of a proper branching condition is difficult and
    it is restricted in their implementation so that it does not refer to
    local variables\footnote{
      their implementation allows free integer variables that are interpreted as universally quantified.
    }.
  Because of this restriction, it cannot find $\formula_b = x \ge 0$.

%}}} End of \subsection{Difference between Suzuki's predicate abstraction}

%}}} End of \section{Definition of Our Predicate Abstraction}
\section{Property} %{{{

This section proves some properties of our predicate abstraction.

\input{PredicateAbstraction/soundness}

\input{PredicateAbstraction/relative_completeness.tex}

%}}} End of \section{Property}
