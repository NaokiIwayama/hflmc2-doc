\subsection{Relative completeness with respect to refinement type system} %{{{
\par
  Though soundness property ensures us the correctness of our algorithm,
  it does not say how good it is.
  This section proves \emph{relative completeness} which guarantees that
  our predicate abstraction has at least the same verification power as
  refinement intersection type system.
\subsubsection{Refinement intersection type system for \nuHFLz} %{{{
  We defines refinement type system for \nuHFLz.
  This is a modified version of refinement intersection type system for
  higher-order functional language~\cite{DependentIntersectionType}.
\par
  The refinement type is defined by the following grammar.
  \begin{alignat*}{3}
    &(\text{type})\quad&
      \rty &::= \tBool
           \mid x:\{ v : \tInt \mid \formula\} \to \rty
           \mid \rtys \to \rty \\
    &(\text{intersection type})\quad&
      \rtys &::= \rty_1 \wedge ... \wedge \rty_k \\
    &(\text{extended type})\quad&
      \ex{\rtys} &::= \tInt \mid \rtys \\
    &(\text{formula})\quad&
      \formula &::= \true \mid \false
               \mid \pred(\tilde{a})
               \mid \formula_1 \land \formula_2
               \mid \formula_1 \lor  \formula_2 \\
    &(\text{environment})\quad&
      \RefTyEnv &::= \emptyset
                \mid \RefTyEnv, x:\ex{\rtys}
  \end{alignat*}
  We require the same well-formedness condition as abstraction type.
  We write $x:\formula \to \rty$ for the abbreviation of
      $x : \{ v:\tInt\mid [v/x]\formula\} \to \rty$.
  Note that the syntax of formula $\formula$ is the same as that of the predicate $P$
  and thus compatible. In the intersection $\rty_1 \wedge ... \wedge \rty_k$,
  each $\delta_i$ is a refinement of the same simple type. Therefore, we can define
  a function $\mathrm{sty}(\_)$ from refinement types to simple types as follows.
  \begin{align*}
    \mathrm{sty}(\tBool) &= \tBool \\
    \mathrm{sty}(\tInt) &= \tInt \\
    \mathrm{sty}(x:\{ v : \tInt \mid \formula\} \to \rty) &= \tInt \to \mathrm{sty}(\rty) \\
    \mathrm{sty}(\rtys \to \rty) &= \mathrm{sty}(\rtys) \to \mathrm{sty}(\rty) \\
    \mathrm{sty}(\rty_1 \wedge ... \wedge \rty_k) &= \sty \text{ (if $\mathrm{sty}(\rty_i)=\sty$ for each $i$)}
  \end{align*}
  We write $\vdash \rty :: \sty$ if $\sty = \mathrm{sty}(\rty)$.
\par
  The typing relation $\AbstTyEnv\ctxSep\formula \vdash \psi : \rty$ is defined in Figure~\ref{rules:ref}.
  This relation means that $\psi$ has refinement type $\rty$ under the environment $\AbstTyEnv$ 
    if formula $\formula$ holds.
  % TODO 空のintersectionで非決定性が生まれるけど問題になることはないはずだから触れないでいいか
  Each rule is self-explanatory. The proof of soundness of this system is given in
  Appendix~\ref{appendix:soundness}.
  % \TODO{各ruleの説明？不要な気もする}
  \begin{figure}[ht!] %{{{ Refinement type system
    \newcommand{\tmpskip}{8pt}
    \infrule[R-Var]
      {x : \rty \in \RefTyEnv}
      {\RefTyEnv\ctxSep\formula \vdash x: \rty}
      \vspace{\tmpskip}
    \infrule[R-Pred]
      {\models \formula \implies \pred(\tilde{a})}
      {\RefTyEnv \ctxSep \formula \vdash \pred(\tilde{a}): \tBool}
      \vspace{\tmpskip}
    \infrule[R-IntApp]
      {\RefTyEnv \ctxSep \formula
        \vdash \psi : (x:\formula' \to \rty)}
      {\RefTyEnv \ctxSep \formula \land [a/x]\formula'
        \vdash \psi a : [a/x]\rty}
      \vspace{\tmpskip}
    \infrule[R-IntAbs]
      {\RefTyEnv,x:\tInt \ctxSep \formula \land \formula'
        \vdash \psi : \rty}
      {\RefTyEnv \ctxSep \formula
        \vdash \lambda x. \psi : (x:\formula' \to \rty)}
      \vspace{\tmpskip}
    \infrule[R-App]
      {\RefTyEnv \ctxSep \formula
        \vdash \psi_1 : \rty_1\wedge...\wedge\rty_k\to\rty \andalso
       \RefTyEnv \ctxSep \formula
        \vdash \psi_2 : \rty_i \text{ (for each $i$)}}
      {\RefTyEnv \ctxSep \formula
        \vdash \psi_1\psi_2 : \rty}
      \vspace{\tmpskip}
    \infrule[R-Abs]
      {\RefTyEnv,x:\rty_1,...,x:\rty_k \ctxSep \formula
        \vdash \psi : \rty}
      {\RefTyEnv \ctxSep \formula
        \vdash \lambda x. \psi : \rty_1\wedge...\wedge\rty_k\to\rty}
      \vspace{\tmpskip}
    \infrule[R-And]
      {\RefTyEnv \ctxSep \formula_1
        \vdash \psi_1 : \rty \andalso
       \RefTyEnv \ctxSep \formula_2
        \vdash \psi_2 : \rty}
      {\RefTyEnv \ctxSep \formula_1 \land \formula_2
        \vdash \psi_1\land\psi_2 : \rty}
      \vspace{\tmpskip}
    \infrule[R-Or]
      {\RefTyEnv \ctxSep \formula_1
        \vdash \psi_1 : \rty \andalso
       \RefTyEnv \ctxSep \formula_2
        \vdash \psi_2 : \rty}
      {\RefTyEnv \ctxSep \formula_1 \lor \formula_2
        \vdash \psi_1\lor\psi_2 : \rty}
      \vspace{\tmpskip}
    \infrule[R-Nu]
      {\RefTyEnv,x:\rty \ctxSep \formula
        \vdash \psi : \rty}
      {\RefTyEnv \ctxSep \formula
        \vdash \nu x.\psi : \rty}
      \vspace{\tmpskip}
    \infrule[R-Coerce]
      {\RefTyEnv \ctxSep \formula
        \vdash \psi : \rty \andalso
       \RefTyEnv \ctxSep \formula
        \vdash \rty \preceq \rty'}
      {\RefTyEnv \ctxSep \formula
        \vdash \psi : \rty'}
      \vspace{\tmpskip}
    \infrule[R-Intersection]
      {\RefTyEnv \ctxSep \formula
        \vdash \psi : \rty_i \text{ (for each $i$)}}
      {\RefTyEnv \ctxSep \formula
        \vdash \psi : \rty_1\wedge...\wedge\rty_k}
      \vspace{\tmpskip}
    \infrule[RSub-Base]
      {}
      {\RefTyEnv \ctxSep \formula
        \vdash \tBool \preceq \tBool}
      \vspace{\tmpskip}
    \infrule[RSub-IntArrow]
      {\models \formula\land\formula_x'\implies\formula_x \andalso
       \RefTyEnv,x:\tInt \ctxSep \formula\land\formula_x'
        \vdash \rty \preceq \rty'}
      {\RefTyEnv \ctxSep \formula
        \vdash (x:\formula_x\to\rty) \preceq (x:\formula'_x\to\rty')}
      \vspace{\tmpskip}
    \infrule[RSub-Arrow]
      {\RefTyEnv \ctxSep \formula
        \vdash \theta' \preceq \theta \andalso
       \RefTyEnv \ctxSep \formula
        \vdash \rty \preceq \rty'}
      {\RefTyEnv \ctxSep \formula
        \vdash  \theta \to\rty
        \preceq \theta'\to\rty'}
      \vspace{\tmpskip}
    \infrule[RSub-Intersection]
      {\text{for each $i$ there exists $i'$ s.t. }
       \RefTyEnv \ctxSep \formula
        \vdash \rty_i \preceq \rty'_{i'}}
      {\RefTyEnv \ctxSep \formula
        \vdash  \rty_1\wedge...\wedge\rty_k
        \preceq \rty'_1\wedge...\wedge\rty'_{k'}}
      \vspace{\tmpskip}
    \infrule[RHes]
      {\RefTyEnv = F_0 : \rtys_0(=\tBool), ..., F_n : \rtys_n \andalso
       \RefTyEnv \ctxSep \true
        \vdash \psi_i : \rtys_i \text{ for each $i$}}
      {\vdash (F_0 =_\nu \psi_0   ; ...; F_n =_\nu \psi_n) : \RefTyEnv}
  \caption{Refinement type system}
  \label{rules:ref}
  \end{figure} %}}} End of refinement type system
%}}} End of \subsubsection{Refinement intersection type system for \nuHFLz}
\subsection{Proof of Relative Completeness} %{{{
\newcommand{\template}{C}
\newcommand{\tty}{\xi}
\newcommand{\ttys}{\chi}
\newcommand{\TemplateTyEnv}{\Xi}
\newcommand{\hole}{[\,]}
  The statement of relative completeness is following
  \begin{Definition}[Relative completeness]
    Let $D_\psi$ be a \nuHFLz formula.
    If $\vdash D_\psi : \RefTyEnv$ for some $\RefTyEnv$,
    there exists $\AbstTyEnv$ and \nuHFL $D_\varphi$ such that
      $\vdash D_\psi : \AbstTyEnv \absInto D_\varphi$ and
      $D_\varphi$ is valid.
  \end{Definition}
\par
  The proof follows that of Kobayashi et al\@.~\cite{MoCHi}.
  We first define \emph{type templates}.
  \begin{Definition}[Type template]
    \begin{alignat*}{3}
      &(\text{template})\quad&
        \template &::= \true \mid \false
                  \mid \hole_i
                  \mid \template_1 \land \template_2
                  \mid \template_1 \lor  \template_2 \\
      &(\text{type})\quad&
        \tty &::= \tBool
             \mid x:\template \to \tty
             \mid \ttys \to \tty \\
      &(\text{intersection type})\quad&
        \ttys &::= \tty_1 \wedge ... \wedge \tty_k \\
      &(\text{extended type})\quad&
        \ex{\ttys} &::= \tInt \mid \ttys \\
      &(\text{environment})\quad&
        \TemplateTyEnv &::= \emptyset
                       \mid \TemplateTyEnv, x:\ex{\ttys}
    \end{alignat*}
  \end{Definition}
  Type templates are used to obtain refinement type by combination with
  abstraction types (template application).
  \begin{Definition}[Template application]
    When template $\template$ does not have any hole $\hole_i$ with $i > k$,
    the template application $\template[\formula_1,...,\formula_k]$ denotes
    formula obtained by replacing all $\hole_i$ in $\template$ with $\formula_i$.
    The refinement type $\tty[\aty]$ and
    the refinement type environment $\TemplateTyEnv[\AbstTyEnv]$
    are also defined by recursively applying template applications. Formally,
    \begin{align*}
      \tBool[\tBool] &= \tBool \\
      (x:\template \to \tty)[x:\tInt[\tilde{P}]\to\aty] &=
        x:\template[\tilde{P}] \to \tty[\aty]\\
      (\ttys \to \tty)[\aty' \to \aty] &=
        \ttys[\aty'] \to \tty[\aty]\\
      (\tty_1\wedge...\wedge\tty_k)[\aty] &=
        \tty_1[\aty]\wedge...\wedge\tty_k[\aty]\\
      \emptyset[\emptyset] &=
        \emptyset \\
      (\TemplateTyEnv,x:\tInt)[\AbstTyEnv,x:\tInt] &=
        \TemplateTyEnv[\AbstTyEnv], x:\tInt\\
      (\TemplateTyEnv,x:\tty)[\AbstTyEnv,x:\aty] &=
        \TemplateTyEnv[\AbstTyEnv], x:\tty[\aty]
    \end{align*}
  \end{Definition}
  Note that, for all refinement type $\rty$ (resp. refinement type environment $\RefTyEnv$),
  there exist $\tty$ and $\aty$ (resp. $\TemplateTyEnv$ and $\AbstTyEnv$) such that
  $\rty = \tty[\aty]$ (resp. $\RefTyEnv = \TemplateTyEnv[\AbstTyEnv]$).
\par
  Given type application $\tty[\aty]$, we define a set $\eval{\tty;\aty}$ of abstract semantic values.
  \begin{Definition} %[Abstract values of templates]
    For a closed template type $\tty$ and a closed abstraction type $\aty$,
    a set of abstract values $\eval{\tty;\aty} \in \Domain{\aty^\flat}$
    is defined by
    \begin{align*}
      \eval{\tBool;\tBool} &=
        \{ \vtrue \} \\
      \eval{(x:\template\to\tty)
           ;(x:\tInt[P_1,...,P_k]\to\aty)} &=
        \left\{ g \;\middle|\;
            \begin{minipage}{17em}
              for all $v_1,...,v_k\in\{\vtrue,\vfalse\}$ and for all $n$, \\
              if $v_i \sqsubseteq_\tBool \eval{P_i}_{\{x\mapsto n\}}$
              and $\eval{C[\tilde{b}]}_{\{\tilde{b}\mapsto\tilde{v}\}}=\vtrue$ \\
              then $g \tilde{v} \in \eval{\tty; \aty_{\{x\mapsto n\}} }$
            \end{minipage}
        \right\} \\
      \eval{\ttys\to\tty;\aty'\to\aty} &=
        \left\{ g \;\middle|\;
            \text{for all $v \in \eval{\ttys;\aty'}$, $g(v) \in \eval{\tty;\aty}$}
        \right\} \\
      \eval{(\tty_1\wedge...\wedge\tty_k);\aty} &=
        \eval{\tty_1;\aty} \cap ... \cap \eval{\tty_k;\aty}
    \end{align*}
    For readability, we write $\eval{C[\tilde{v}]}$ instead of
      $\eval{C[\tilde{b}]}_{\{\tilde{b}\mapsto\tilde{v}\}}$.
    A set of (abstract) valuation
    $\eval{\TemplateTyEnv;\AbstTyEnv|\template;\predSet}$
    is defined by following; a valuation $\rho$
      (which respects $(\AbstTyEnv,\predSet)^\flat$)
    is a member of
    $\eval{\TemplateTyEnv;\AbstTyEnv|\template;\predSet(=P_1,...,P_k)}$
    if and only if there exists an integer assignment $\eta$ which satisfies
    \begin{itemize}
      \item $\dom{\eta} = \{ x \mid x:\tInt \in \AbstTyEnv \}$,
      \item $\rho(x) \in \eval{\TemplateTyEnv(x); (\AbstTyEnv(x))_\eta}$
            for all $x \in \dom{\AbstTyEnv}$,
      \item $\rho(b_{P_i}) \sqsubseteq_\tBool \eval{P_i}_\eta$ for each $i$, and
      \item $\eval{C[b_{P_1},...,b_{P_k}}_\rho = \vtrue$
    \end{itemize}
    For $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv|\template;\predSet}$,
    $\eta_\rho$ denotes the integer assignment which satisfies the above conditions.
  \end{Definition}
  The following lemma is the key of the proof.
  \begin{Lemma}\label{lem:relative_completeness}
    If $\TemplateTyEnv[\AbstTyEnv]\ctxSep\template[\predSet]\vdash\psi:\tty[\rty]$,
    there exists HFL formula $\varphi$ such that
    \begin{itemize}
      \item $\AbstTyEnv\ctxSep\predSet\vdash \psi : \aty \absInto \varphi$
      \item $\eval{\varphi}_\rho \in \eval{\tty;\aty_{\eta_\rho}}$
            for any $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv|\template;\predSet}$.
    \end{itemize}
  \end{Lemma}
  \begin{proof*} %{{{
    By induction on the derivation with the case analysis of the last rule used.
    \begin{itemize}
      \item \inflabel{R-Var}. %{{{
        $\varphi = x$ satisfies required conditions.
        %}}}
      \item \inflabel{R-Pred}. %{{{
        Let $P_1,...,P_k = \predSet$.
        Because $\models C[\tilde{P}] \implies \pred(\tilde{a})$ holds, we have the following derivation
        \begin{align*}
          \infer
            {\AbstTyEnv\ctxSep\predSet\vdash \pred(\tilde{a}) : \tBool \absInto C[\tilde{b}_P]}
            {\AbstTyEnv\ctxSep\predSet,\pred(\tilde{a}) \vdash 
                \pred(\tilde{a}) \absInto b_{\pred(\tilde{a})} \andalso
             \AbstTyEnv \vdash b_{\pred(\tilde{a})}
                :        ((\predSet,\pred(\tilde{a})), \tBool)
                \subType (\predSet, \tBool)
                \absInto C[\tilde{b}_P]}
        \end{align*}
        By the definition of $\eval{\TemplateTyEnv;\AbstTyEnv|\template;\predSet}$,
        $C[\tilde{b}_P]_\rho = \vtrue \in \eval{\tBool;\tBool}$.
        %}}}
      \item \inflabel{R-IntApp}. %{{{
        The bottom of the derivation tree has the following form.
        \begin{align*}
          \infer
            {\TemplateTyEnv[\AbstTyEnv]\ctxSep C[\predSet]\land C_x[[a/x]\tilde{P}] \vdash
                \psi a : \tty[[a/x]\aty]
            }
            {\TemplateTyEnv[\AbstTyEnv]\ctxSep C[\predSet] \vdash
                \psi : (x:C_x\to\tty)[x:\tInt[\tilde{P}]\to\aty]
            }
        \end{align*}
        By induction hypothesis, there exists $\varphi$ such that
          $\AbstTyEnv\ctxSep\predSet \vdash \psi : x:\tInt[\tilde{P}]\to\aty \absInto \varphi$ and
          $\eval{\varphi}_\rho \in \eval{x:C_x\to\tty; x:\tInt[\tilde{P}_{\eta_\rho}]\to\aty_{\eta_\rho}}$
          for any $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv \ctxSep C;\predSet}$.
        We have
          $\AbstTyEnv\ctxSep\predSet,[a/x]\tilde{P} \vdash 
            \psi a : [a/x]\aty \absInto \varphi \tilde{b}_{[a/x]P}
          $.
        Fix any $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C\land C_x; \predSet,[a/x]\tilde{P}}$.
        Let us fix such $\rho$. Note that $\rho$ is also in
          $\eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C; \predSet}$.
        Let $v_i = \rho(b_{[a/x]P_i})$ and $n = \eval{a}_{\eta_\rho}$.
        We have
        \begin{itemize}
          \item
            $\eval{\varphi}_\rho \in \eval{x:C_x\to\tty; x:\tInt[\tilde{P}_{\eta_\rho}]\to\aty_{\eta_\rho}}$
            (by induction hypothesis)
          \item
            $v_i \sqsubseteq_\tBool \eval{[a/x]P_i}_{\eta_\rho} = \eval{P_i}_{\eta_\rho \cup \{x\mapsto n\}}$
            (by definition of $\eta_\rho$)
          \item
            $\eval{C[\tilde{b}]}_{\{\tilde{b}\mapsto\tilde{v}\}}$
            (by definition of $\rho$)
        \end{itemize}
        Thus,
          $\eval{\varphi \tilde{b}_{[a/x]P}}_\rho = \eval{\varphi}_\rho\tilde{v}
                \in \eval{\tty; \aty_{\eta_\rho\cup\{x\mapsto n\}} }$
        as required.
        %}}}
      \item \inflabel{R-IntAbs}. %{{{
        The bottom of the derivation tree has the following form.
        \begin{align*}
          \infer
            {\TemplateTyEnv[\AbstTyEnv]\ctxSep C[\predSet] \vdash
                \lambda x. \psi : (x:C_x\to\tty)[x:\tInt[\tilde{P}]\to\aty]
            }
            {(\TemplateTyEnv,x:\tInt)[\AbstTyEnv,x:\tInt]\ctxSep C[\predSet]\land C_x[\tilde{P}] \vdash
                \psi : \tty[\aty]
            }
        \end{align*}
        By induction hypothesis, there exists $\varphi$ such that
          $\AbstTyEnv,x:\tInt\ctxSep\predSet,\tilde{P}\vdash\psi : \aty \absInto \varphi$
        and
          $\eval{\varphi}_{\rho'} \in \eval{\tty;\aty_{\eta_{\rho'}} }$
        for any
          $\rho' \in \eval{(\TemplateTyEnv,x:\tInt);(\AbstTyEnv,x:\tInt)\ctxSep C\land C_x;\predSet,\tilde{P}}$.
        We have
          $\AbstTyEnv\ctxSep\predSet\vdash
            \lambda x.\psi : x:\tInt[\tilde{P}]\to\aty \absInto \lambda\tilde{b}_P.\varphi$
        We show 
          $\eval{\lambda\tilde{b}_P.\varphi}_\rho \in \eval{x:C_x\to\tty;x:\tInt[\tilde{P}]\to\aty}$
        for any
          $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C;\predSet}$.
        Let us fix $\tilde{v}$ and $n$ that satisfies
          $v_i \sqsubseteq_\tBool \eval{P_i}_{\eta_\rho\cup\{x\mapsto n\}}$
        and
          $\eval{C_x[\tilde{b}]}_{\{\tilde{b}\mapsto\tilde{v}\}} = \vtrue$.
        In addition, let $\rho' = \rho\cup\{\tilde{b}_P\mapsto\tilde{v}\}$.
        Since
          $\rho' \in \eval{(\TemplateTyEnv,x:\tInt);(\AbstTyEnv,x:\tInt)\ctxSep C\land C_x;\predSet,\tilde{P}}$
          (with $\eta_{\rho'} = \eta_\rho\cup\{x\mapsto n\}$),
        we have
        \begin{align*}
          \eval{\lambda \tilde{b}_P.\varphi}_\rho \tilde{v}
          =   \eval{\varphi}_{\rho'}
          \in \eval{\tty;\aty_{\eta_{\rho'}} }
          =   \eval{\tty,\aty_{\eta_\rho\cup\{x\mapsto n\}} }
        \end{align*}
        as required.
        %}}}
      \item \inflabel{R-App}, \inflabel{R-Abs}, \inflabel{R-And}, \inflabel{R-Or}. %{{{
        By straightforward application of induction hypothesis.
        %}}}
      \item \inflabel{R-Nu}. %{{{
        By induction hypothesis, we have
          $\eval{\lambda x.\varphi}_\rho v \in \eval{\tty;\aty_{\eta_\rho}}$
        for all
          $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C;\predSet}$ and
          $v \in \eval{\tty;\aty_{\eta_\rho}}$.
        Let us fix $\rho$ and $f$ be $\eval{\lambda x.\varphi}_\rho$.
        The goal is to show $\mathrm{gfp}(f) \in \eval{\tty;\aty_{\eta_\rho}}$.
        Because semantic domain is finite, there exists natural number $n$
        such that $\mathrm{gfp}(f) = f^n(\top)$.
        It is easy to show $f^n(\top) \in \eval{\tty;\aty_{\eta_\rho}}$ for all $n$ (by induction).
        %}}}
      \item \inflabel{R-Coerce}. By the following lemma %{{{
        \QED
      %}}}
    \end{itemize}
  \end{proof*}%}}}
  \begin{Lemma}
    Suppose that
    \begin{itemize}
      \item
        $\models C'[\predSet'] \implies C[\predSet]$
      \item
        % $\TemplateTyEnv[\AbstTyEnv]\ctxSep C[\predSet] \vdash \tty[\aty]\subType\tty'[\aty']$
        $\TemplateTyEnv[\AbstTyEnv]\ctxSep C'[\predSet'] \vdash \tty[\aty]\subType\tty'[\aty']$
      \item
        $(\AbstTyEnv\ctxSep\predSet)^\flat \vdash \varphi : \aty^\flat$
      \item
        $\eval{\varphi}_\rho \in \eval{\tty;\aty_{\eta_\rho}}$
        for all $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C;\predSet}$
    \end{itemize}
    There exists $\varphi'$ such that
    \begin{itemize}
      \item
        $\AbstTyEnv \vdash \varphi : (\predSet,\aty) \subType (\predSet',\aty') \absInto \varphi'$
      \item
        $\eval{\varphi'}_{\rho'} \in \eval{\tty;\aty_{\eta_{\rho'}} }$
        for all $\rho' \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C';\predSet'}$
    \end{itemize}
  \end{Lemma}
  \begin{proof*} %{{{
    Induction on the derivation with case analysis on the last rule used.
    \begin{itemize}
      \item \inflabel{RSub-Base} %{{{
        Let $P_1,...,P_k = \predSet$ and $Q_1,...,Q_l = \predSet'$.
        Let us define function $f$ by
        \begin{align*}
          f(\tilde{v}') = \{
            \tilde{v} \mid \exists \eta.\;
              \tilde{v'} \sqsubseteq_\tBool \eval{\tilde{Q}}_\eta \text{, }
              % \tilde{v} =\eval{\tilde{P}}_\eta
              \tilde{v} \sqsubseteq_\tBool \eval{\tilde{P}}_\eta \text{ and }
              \eval{C[\tilde{v}]} = \vtrue
          \}
        \end{align*}
        Note that $f(\tilde{v'})$ is non-empty if $\eval{C'[\tilde{v'}]} = \vtrue$ because
        $\eval{C'[\tilde{v'}]}
          \sqsubseteq_\tBool \eval{C'[\tilde{Q}]}_{\eta}
          \sqsubseteq_\tBool \eval{C [\tilde{P}]}_{\eta}$
        for some $\eta$, and thus $\eval{\tilde{P}}_\eta$ is in $f(\tilde{v'})$.
        Using this function, we define $\xi$ as follows.
        \begin{align*}
          \xi = \lambda \tilde{b}_Q.\;
            \bigvee_{\tilde{v}': \eval{C'[\tilde{v}']}=\vtrue} \left(
              \bigwedge_{i: v'_i=\vtrue} b_{Q_i}
              \wedge
              \bigwedge_{\tilde{v} \in f(\tilde{v'})}
                X \tilde{v}
            \right)
        \end{align*}
        This $\xi$ satisfies
          $\eval{\xi \tilde{Q}}_{\rho^*} \sqsubseteq_\tBool \eval{X \tilde{P}}_{\rho^*}$.
        To prove this, it is sufficient to show
          $\eval{
            \bigwedge_{i: v'_i=\vtrue} b_{Q_i}
              \wedge
            \bigwedge_{\tilde{v} \in f(\tilde{v'})}
              X \tilde{v}}_{\rho^*}
          \sqsubseteq_\tBool \eval{X\tilde{P}}_{\rho^*}$
        for each $\tilde{v'}$ such that $\eval{C'[\tilde{v}']}=\vtrue$.
        If $\tilde{v'} \sqsubseteq_\tBool \eval{\tilde{Q}}_\eta$,
          then $\eval{\tilde{P}}_{\rho^*} \in f(\tilde{v'})$ and thus the above inequation holds.
        Otherwise, the left side is false and thus it holds.
        Therefore, there is a derivation of
          $\AbstTyEnv \vdash \varphi : (\tilde{P},\tBool) \subType (\tilde{Q},\tBool) \absInto 
            [\lambda \tilde{b}_P. \varphi/ X]\xi \tilde{b}_Q$.
        Let $\varphi' = [\lambda \tilde{b}_P. \varphi/ X]\xi \tilde{b}_Q$ and
            take $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C';\predSet'}$.
        We have
        \begin{align*}
          \eval{\varphi'}_{\rho} &=
            \eval{\xi\tilde{b}_Q}_{\rho\cup\{X\mapsto \eval{\lambda\tilde{b}_P.\varphi}_\rho\}}
          \\&\sqsupseteq_\tBool
            \eval{ \bigwedge_{\tilde{v} \in f(\tilde{v'})}
              X \tilde{v} }_{\rho\cup\{X\mapsto \eval{\lambda\tilde{b}_P.\varphi}_\rho\}}
            &\text{($\tilde{v'} = \eval{\tilde{Q}}_\rho$ )}
          \\&=
            \bigsqcap_{\tilde{v} \in f(\tilde{v'})}
            \eval{ X \tilde{v} }_{\rho\cup\{X\mapsto \eval{\lambda\tilde{b}_P.\varphi}_\rho\}}
          \\&=
            \bigsqcap_{\tilde{v} \in f(\tilde{v'})}
            \eval{ \varphi }_{\rho\cup\{\tilde{b}_P\mapsto\tilde{v}\}}
          \\&=
            \bigsqcap_{\tilde{v} \in f(\tilde{v'})} \vtrue
            &(\because \rho\cup\{\tilde{b}_P\mapsto\tilde{v}\} \in
                  \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C;\predSet})
          \\&= \vtrue
            &(\because f(\tilde{v'}) \neq \emptyset)
        \end{align*}
        %}}}
      \item \inflabel{RSub-IntArrow}. %{{{
        The bottom of the derivation tree has the following form.
        \begin{align*}
          \infer
            {\TemplateTyEnv[\AbstTyEnv]\ctxSep C[\predSet] \vdash
                (x:C_x \to\tty )[x:\tInt[\tilde{P}]\to\aty ] \subType
                (x:C_x'\to\tty')[x:\tInt[\tilde{Q}]\to\aty']
            }
            {\models C[\predSet]\land C_x'[\tilde{Q}] \implies C_x[\tilde{P}]
              \andalso
             (\TemplateTyEnv,x:\tInt)[\AbstTyEnv,x:\tInt]\ctxSep C[\predSet]\land C_x'[\tilde{Q}] \vdash
                \tty [\aty ] \subType
                \tty'[\aty']
            }
        \end{align*}
        Let $\varphi$ be a formula which satisfies the assumption of this lemma.
        We have
        \begin{itemize}
          \item
            $\models C'[\predSet'] \land C'_x[\tilde{Q}]
                \implies C[\predSet] \land C_x[\tilde{P}]$
          \item
            $(\TemplateTyEnv,x:\tInt)[\AbstTyEnv,x:\tInt]\ctxSep 
                C'[\predSet'] \land C'_x[\tilde{Q}] \vdash 
                \tty[\aty]\subType\tty'[\aty']$
            (by weakening)
          \item
            $(\AbstTyEnv\ctxSep\predSet,\tilde{P})^\flat \vdash \varphi\tilde{b}_P : \aty^\flat$
          \item
            $\eval{\varphi\tilde{b}_P}_\rho \in \eval{\tty;\aty_{\eta_\rho}}$
            for all $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C\land C_x;\predSet,\tilde{P}}$
        \end{itemize}
        Thus, by induction hypothesis, there exists $\varphi'$ such that
        \begin{itemize}
          \item
            $\AbstTyEnv \vdash \varphi \tilde{b}_P: 
                (\predSet,\aty) \subType (\predSet',\aty') \absInto \varphi'$
          \item
            $\eval{\varphi'}_{\rho'} \in \eval{\tty;\aty_{\eta_{\rho'}} }$
            for all $\rho' \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep C'\land C'_x;\predSet',\tilde{Q}}$
        \end{itemize}
        By the same procedure as in the case $\inflabel{R-IntAbs}$, we can show that
        $\lambda \tilde{b}_Q. \varphi'$ satisfies the required condition.
        %}}}
      \item \inflabel{RSub-Arrow}. %{{{
        The bottom of the derivation tree has the following form.
        \begin{align*}
          \infer
            {\TemplateTyEnv[\AbstTyEnv]\ctxSep C[\predSet] \vdash
                (\ttys_1 \to\tty_2 )[\aty_1 \to\aty_2 ] \subType
                (\ttys_1'\to\tty_2')[\aty_1'\to\aty_2']
            }
            {\TemplateTyEnv[\AbstTyEnv]\ctxSep C[\predSet] \vdash
                \ttys_1'[\aty_1'] \subType
                \ttys_1 [\aty_1 ]
              \andalso
             \TemplateTyEnv[\AbstTyEnv]\ctxSep C[\predSet]\vdash
                \tty_2 [\aty_2 ] \subType
                \tty_2'[\aty_2']
            }
        \end{align*}
        We have
        \begin{itemize}
          \item
            $\models C'[\predSet'] \implies C[\predSet]$
          \item
            $(\TemplateTyEnv,x:\ttys_1')[\AbstTyEnv,x:\aty_1']\ctxSep C'[\predSet'] \vdash
              \ttys_1'[\aty_1'] \subType \ttys_1 [\aty_1 ] $
            (by weakening)
          \item
            $(\AbstTyEnv,x:\aty_1'\ctxSep\predSet)^\flat \vdash x : (\aty_1')^\flat$
          \item
            $\eval{x}_\rho \in \eval{\tty;(\aty_1')_{\eta_\rho}}$
            for all $\rho \in \eval{\TemplateTyEnv,x:\ttys_1';\AbstTyEnv,x:\aty_1' \ctxSep C;\predSet}$
        \end{itemize}
        By induction hypothesis\footnote{
          extension to intersection is easy
        }, there exists $\varphi_1$ such that
        \begin{itemize}
          \item
            $\AbstTyEnv, x:\aty_1' \vdash x :
                (\predSet ,\aty_1') \subType 
                (\predSet',\aty_1 ) \absInto \varphi_1$
          \item
            $\eval{\varphi_1}_{\rho'} \in \eval{\tty;(\aty_1)_{\eta_{\rho'}} }$
            for all $\rho' \in \eval{\TemplateTyEnv,x:\ttys_1';\AbstTyEnv,x:\aty_1' \ctxSep C';\predSet'}$
        \end{itemize}
        On the other hand, we have
        \begin{itemize}
          \item
            $\models C'[\predSet'] \implies C[\predSet]$
          \item
            $(\TemplateTyEnv,y:\ttys_1)[\AbstTyEnv,y:\aty_1]\ctxSep C'[\predSet'] \vdash 
              \tty_2[\aty_2] \subType \tty_2' [\aty_2'] $
            (by weakening)
          \item
            $(\AbstTyEnv,y:\aty_1\ctxSep\predSet,\predSet')^\flat \vdash \varphi y : \aty_2^\flat$
          \item
            $\eval{\varphi y}_\rho \in \eval{\tty;(\aty_2)_{\eta_\rho}}$
            for all $\rho \in \eval{\TemplateTyEnv,y:\ttys_1;\AbstTyEnv,y:\aty_1
                            \ctxSep C;\predSet}$
        \end{itemize}
        By induction hypothesis again, there exists $\varphi_2$ such that
        \begin{itemize}
          \item
            $\AbstTyEnv, y:\aty_1 \vdash \varphi y :
                (\predSet ,\aty_2) \subType 
                (\predSet',\aty_2') \absInto \varphi_2$
          \item
            $\eval{\varphi_2}_{\rho'} \in \eval{\tty;(\aty_2')_{\eta_{\rho'}} }$
            for all $\rho' \in \eval{\TemplateTyEnv,y:\ttys_1;\AbstTyEnv,y:\aty_1 \ctxSep C';\predSet'}$
        \end{itemize}
        It is easy to confirm that $\lambda x. [\varphi_1/y]\varphi_2$ satisfies the required conditions.
        \QED
        %}}}
    \end{itemize}
  \end{proof*}%}}}
  Relative completeness is proven using Lemma~\ref{lem:relative_completeness}.
  \begin{Theorem}
    Let $D_\psi$ be a \nuHFLz formula.
    If $\vdash D_\psi : \TemplateTyEnv[\AbstTyEnv]$,
    there exists pure HFL formula $D_\varphi$ such that
    $\AbstTyEnv\vdash D_\psi \absInto D_\varphi$ and
    $D_\varphi$ is valid.
  \end{Theorem}
  \begin{proof*}
    Let $\{F_0 =_\nu \psi_0;...;F_n=_\nu \psi_n\} = D_\psi$,
        $\aty_i = \AbstTyEnv(F_i)$, and
        $\ttys_i = \TemplateTyEnv(F_i)$.
    By Lemma~\ref{lem:relative_completeness}, there exists $\varphi_i$ such that
    \begin{enumerate}[(i)]
      \item
        $\AbstTyEnv\ctxSep\varepsilon\vdash\psi_i:\aty_i\absInto\varphi_i$
      \item
        $\eval{\varphi_i} \in \eval{\ttys_i;\aty_i}$ for any
        $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep\true;\varepsilon}$.
    \end{enumerate}
    Let $D_\varphi = \{F_0 =_\nu \varphi_0;...;F_n =_\nu \varphi_n\}$.
    Then, $\vdash D_\psi : \AbstTyEnv \absInto D_\varphi$ holds.
    As described in the Chapter~\ref{chapter:Preliminaries}, it is known that
    $\eval{D_\varphi}$ is equal to $\rho^*(F_0)$ where
    $\rho^*$ is the greatest fixpoint of the following function $f$:
    \begin{align*}
      f(\rho) = \{ X \mapsto \eval{\varphi}_\rho \mid X =_\nu \varphi \in D_\varphi\}
    \end{align*}
    So, it is sufficient to show
      $\rho^* \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep\true;\varepsilon}$.
    Since semantic domain is finite, there exists natural number $n$ such that
    $\rho^* = f^n(\top)$. Here, valuation $\top$ maps $F_i^{\sty_i}$ to $\top_{\sty_i}$ for each $i$
    and is in $\eval{\TemplateTyEnv;\AbstTyEnv\ctxSep\true;\varepsilon}$.
    By (ii), $f(\rho) \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep\true;\varepsilon}$
      if $\rho \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep\true;\varepsilon}$.
    Thus, $\rho^* \in \eval{\TemplateTyEnv;\AbstTyEnv\ctxSep\true;\varepsilon}$.
  \end{proof*}
%}}} End of \subsection{Relative completeness with respect to refinement type system}
%}}}
