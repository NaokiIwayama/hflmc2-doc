\subsection{Soundness}\label{predicate_abstraction:soundness}
  Soundness is a property that
    the \nuHFL formula $D_\varphi$ obtained by
    our predicate abstraction is an underapproximation of input \nuHFLz formula $D_\psi$.
  This guarantee the correctness of our whole procedure since
  if $D_\varphi$ is valid then so is $D_\psi$ as mentioned in Chapter~\ref{chapter:Introduction}.
\par
  In the proof, we use the same abstract interpretation as Suzuki.
  We first define
    the abstraction function $\alpha$ and
    the concretization function $\gamma$,
    which give us a translation between concrete semantics and abstract semantics.
  \begin{Definition}[abstraction function and concretization function]
    Given abstraction type $\aty$,
      the abstraction function $\alpha_\aty : \Domain{\aty^\sharp}\to\Domain{\aty^\flat}$ and
      the concretization function $\gamma\aty : \Domain{\aty^\flat}\to\Domain{\aty^\sharp}$
    are defined as follows.
    \begin{align*}
      \alpha_\tBool (v) &= v \\
      \gamma_\tBool (v) &= v \\
      \alpha_{x:\tInt[\tilde{P}]\to\aty} (f) &=
        \left\{ \tilde{b} \mapsto
          \bigsqcap\{ \alpha_{[n/x]\aty}(f(z))
                 \mid n \in \mathbb{Z}, \eval{\tilde{P}}_{\{x\mapsto n\}} = \tilde{b} \}
        \right\}
        \\
      \gamma_{x:\tInt[\tilde{P}]\to\aty} (f) &=
        \{ n \mapsto
          \gamma_{[n/x]\aty}(f(\eval{\tilde{P}}_{\{x\mapsto n\}}))
        \}
        \\
      \alpha_{\aty_1\to\aty_2} (g) &= \{ v \mapsto \alpha_{\aty_2} (g (\gamma_{\aty_1} (v)))\} \\
      \gamma_{\aty_1\to\aty_2} (g) &= \{ v \mapsto \gamma_{\aty_2} (g (\alpha_{\aty_1} (v)))\}
    \end{align*}
    % Here, $\eval{P}$ denotes (boolean) value of closed first-order formula $P$.
  \end{Definition}
  These functions has some important properties as described by following lemmas.
  Consult Appendix A of Suzuki~\cite{Suzuki} for proof.
  \begin{Lemma}\label{lemma:AI:mono}
    $\alpha_\aty$ and $\gamma_\aty$ are monotonic.
  \end{Lemma}
  \begin{Lemma}\label{lemma:AI:order}
    For all abstraction type $\aty$, $v \in \Domain{\aty^\flat}$, $w \in \Domain{\aty^\sharp}$,
    \begin{align*}
      v \sqsubseteq_{\aty^\flat} \alpha_\aty(w) \iff
      \gamma_\aty(v) \sqsubseteq_{\aty^\sharp} w
    \end{align*}
  \end{Lemma}
  According to Lemma~\ref{lemma:AI:order}, we can define a order
    $\leqAI_\aty \in \Domain{\aty^\flat} \times \Domain{\aty^\sharp}$
  as follows.
  \begin{align*}
    v \leqAI_\aty w
      \iffdef v \sqsubseteq_{\aty^\flat} \alpha_\aty(w)
      (\iff \gamma_\aty(v) \sqsubseteq_{\aty^\sharp} w)
  \end{align*}
  The fixpoint operator preserves this order.
  The proof is given in Appendix A of Suzuki~\cite{Suzuki} again.
  \begin{Lemma}\label{lemma:AI:gfp}
    If $f \leqAI_{\sigma\to\sigma} g$, then $\mathrm{gfp}(f) \leqAI_{\sigma} \mathrm{gfp}(g)$.
  \end{Lemma}
  We can extend this order to valuations. Let $\rhoA$ (resp. $\rhoC$) be a
  valuation that respect $\AbstTyEnv^\flat$ (resp. $\AbstTyEnv^\sharp$).
  % We define $\leqAI_{\AbstTyEnv,\predSet}$ by
  The relation $\rhoA \leqAI_{\AbstTyEnv,\predSet} \rhoC$ holds if and only if
  \begin{itemize}
    \item $\rhoA(x) \leqAI_{\aty_\rhoC} \rhoC(x)$ for all $x:\aty \in \AbstTyEnv$ and
    \item $\rhoA(b_P) \sqsubseteq_\tBool \eval{P}_\rhoC$ for all $P \in \predSet$
  \end{itemize}
  Here, $\aty_\rhoC$ (resp. $P_\rhoC$) is a result of replacing all free integer variables $x$
  in $\aty$ (resp. $P$) with $\rhoC(x)$.
    % application of substitution $\rhoC$ to $\aty$ and $P$.
  Now we can describe the main lemma.
  \begin{Lemma}
    If $\AbstTyEnv \ctxSep \predSet \vdash \psi : \sigma \absInto \varphi$
    and $\rhoA \;\leqAI_{\AbstTyEnv,\predSet}\; \rhoC$ then
    $\eval{(\AbstTyEnv\ctxSep\predSet)^\flat \vdash \varphi : \aty^\flat}_{\rhoA}
        \leqAI_{\aty_{\rhoC}}
     \eval{\AbstTyEnv^\sharp \vdash \psi : \aty^\sharp}_{\rhoC}$
    % Here, $\sigma_\rhoC$ is an abstraction type obtained by replacing free integer variable $x$ with $\rhoC(x)$.
  \end{Lemma}
  \begin{proof*}%{{{
    Induction on the derivation of $\AbstTyEnv \ctxSep \predSet \vdash \psi : \sigma \absInto \varphi$ with
    case analysis on the last rule used.
    \begin{itemize}
      \item \inflabel{A-Var} and \inflabel{A-Pred}. %{{{
        By definition of $\leqAI_{\AbstTyEnv,\predSet}$.
        %}}}
      \item \inflabel{A-App}, \inflabel{A-Abs}, \inflabel{A-And}, \inflabel{A-Or}. %{{{
        By straightforward application of induction hypothesis.
        %}}}
      \item \inflabel{A-IntApp}. %{{{
        Let $\rhoA$ and $\rhoC$ be valuations with
          $\rhoA \leqAI_{\AbstTyEnv,(\predSet,[a/x]\tilde{P})} \rhoC$.
        The goal is to show the following relation.
        \begin{align*}
          \eval{
              (\AbstTyEnv\ctxSep\predSet)^\flat
                \vdash \varphi : \tBool^k\to\aty^\flat
            }_{\rhoA}
            \rhoA(\tilde{b}_{[a/x]P})
              \leqAI_{[a/x]\aty_{\rhoC}}
          \eval{
              \AbstTyEnv^\sharp \vdash \psi : \tInt\to\aty^\sharp
            }_{\rhoC}
            \eval{a}_{\rhoC}
        \end{align*}
        Here, $\tBool^k\to\aty^\flat$ is an abbreviation for
          $\overbrace{\tBool\to\cdots\to\tBool}^{k}\to\aty^\flat$ and
          $\tilde{P} = P_1,...,P_k$.
        Let $f =
          \eval{
              (\AbstTyEnv\ctxSep\predSet)^\flat
                \vdash \varphi : \tBool^k\to\aty^\flat
            }_{\rhoA}$
        and $g =
          \eval{
              \AbstTyEnv^\sharp \vdash \psi : \tInt\to\aty^\sharp
            }_{\rhoC}$ for readability.
        By induction hypothesis, we have
          $f \leqAI_{x:\tInt[\tilde{P}_\rhoC]\to[a/x]\aty_{\rhoC}} g$
        since $\rhoA$ and $\rhoC$ also satisfy $\rhoA \leqAI_{\AbstTyEnv,\predSet} \rhoC$.
        Thus, we have
          $f(\tilde{P}_{\rhoC\cup\{x\mapsto n\}}) \leqAI_{\aty_{\rhoC\cup\{x\mapsto n\}} } g(n)$
        for all $n$.
        Let $n = \eval{a}_\rhoC$.
        Since
          $\rhoA(b_{[a/x]P_i})
           \sqsubseteq_\tBool \eval{[a/x]P_i}_\rhoC
           =                  \eval{P_i}_{\rhoC\cup\{x\mapsto n\}}$
        holds for each $i$,
        we have
        \begin{align*}
          f(\rhoA(\tilde{b}_{[a/x]P}))
            \sqsubseteq_{\aty^\flat}
          f(\tilde{P}_{\rhoC\cup\{x\mapsto n\}})
            \leqAI_{\aty_{\rhoC\cup\{x\mapsto n\}} }
          g(n)
        \end{align*}
        as required, using Lemma~\ref{lemma:AI:mono}.
        %}}}
      \item \inflabel{A-IntAbs}. %{{{
        Let $\rhoA$ and $\rhoC$ be valuations with
          $\rhoA \leqAI_{\AbstTyEnv,\predSet} \rhoC$.
        The goal is to show
        \begin{align*}
          \eval{
              (\AbstTyEnv\ctxSep\predSet)^\flat
                \vdash \lambda \tilde{b}_P. \varphi : \tBool^k\to\aty^\flat
            }_{\rhoA}
              \leqAI_{x:\tInt[\tilde{P_\rhoC}]\to\aty_{\rhoC}}
          \eval{
              \AbstTyEnv^\sharp \vdash \lambda x. \psi : \tInt\to\aty^\sharp
            }_{\rhoC}
        \end{align*}
        which is equivalent to
        \begin{align*}
          \eval{
              (\AbstTyEnv\ctxSep\predSet)^\flat
                \vdash \lambda \tilde{b}_P. \varphi : \tBool^k\to\aty^\flat
            }_{\rhoA}
            (\eval{\tilde{P}}_{\rhoC\cup\{x\mapsto n\}})
              \leqAI_{\aty_{\rhoC\cup\{x\mapsto n\}} }
          \eval{
              \AbstTyEnv^\sharp \vdash \lambda x. \psi : \tInt\to\aty^\sharp
            }_{\rhoC} (n)
          \\ \text{ for all $n$}
            % \eval{a}_{\rhoC}
        \end{align*}
        By definition of $\eval{\lambda x.\psi}$, we have
        \begin{align*}
          &\eval{
              (\AbstTyEnv\ctxSep\predSet)^\flat
                \vdash \lambda \tilde{b}_P. \varphi : \tBool^k\to\aty^\flat
            }_{\rhoA}
            (\eval{\tilde{P}}_{\rhoC\cup\{x\mapsto n\}})
          \\&\quad=
          \eval{
              (\AbstTyEnv\ctxSep\predSet,\tilde{})^\flat
                \vdash \varphi : \aty^\flat
            }_{\rhoA\cup\{\tilde{b}_P \mapsto \eval{\tilde{P}}_{\rhoC\cup\{x\mapsto n\}}\}}
          \\
          &\eval{
              \AbstTyEnv^\sharp \vdash \lambda x. \psi : \tInt\to\aty^\sharp
            }_{\rhoC} (n)
          \\&\quad=
          \eval{
              (\AbstTyEnv,x:\tInt)^\sharp \vdash \psi : \tInt\to\aty^\sharp
            }_{\rhoC\cup\{x\mapsto n\}}
        \end{align*}
        Let $\rhoA' = \rhoA\cup\{\tilde{b}_P \mapsto \eval{\tilde{P}}_{\rhoC\cup\{x\mapsto n\}}\}$ and
            $\rhoC' = \rhoC\cup\{x\mapsto n\}$.
        Then, $\rhoA' \leqAI_{(\AbstTyEnv,x:\tInt),(\predSet,\tilde{P})} \rhoC'$ holds.
        By induction hypothesis, we have
        \begin{align*}
          \eval{
              (\AbstTyEnv\ctxSep\predSet,\tilde{P})^\flat
                \vdash \varphi : \aty^\flat
            }_{\rhoA'}
              \leqAI_{\aty_{\rhoC}}
          \eval{
              (\AbstTyEnv,x:\tInt)^\sharp \vdash \psi : \tInt\to\aty^\sharp
            }_{\rhoC'}
        \end{align*}
        as required.
        %}}}
      \item \inflabel{A-Nu}. %{{{
        By Lemma~\ref{lemma:AI:gfp}.
        %}}}
      \item \inflabel{A-Coerce}. %{{{
          The bottom of the derivation tree is in the following form.
          \begin{align*}
            \infer
              {\AbstTyEnv \ctxSep \predSet
                  \vdash \psi : \aty
                  \absInto \varphi}
              {\AbstTyEnv \ctxSep \predSet'
                  \vdash \psi : \aty'
                  \absInto \varphi' \andalso
               \AbstTyEnv \vdash \varphi
                  :         (\predSet', \aty')
                  \subType  (\predSet , \aty)
                  \absInto \varphi'}
          \end{align*}
        Let $\rhoA$ and $\rhoC$ be valuations with
          $\rhoA \leqAI_{\AbstTyEnv,\predSet} \rhoC$.
        By the following lemma, there exists $\rhoA'$ with
        \begin{itemize}
          \item $\rhoA' \leqAI_{\AbstTyEnv,\predSet'} \rhoC$.
          \item $\eval{\varphi'}_\rhoA' \leqAI_{\aty'_\rhoC} w$ implies
                $\eval{\varphi }_\rhoA  \leqAI_{\aty_\rhoC} w$ for all $w$
                (this is equivalent to
                  $\gamma_{\aty}(\eval{\varphi}_\rhoA) \sqsubseteq_{\aty^\sharp}
                   \gamma_{\aty'}(\eval{\varphi'}_{\rhoA'})$).
        \end{itemize}
        By induction hypothesis, we have
          $\eval{\varphi'}_\rhoA' \leqAI_{\aty'_\rhoC} \eval{\psi}_\rhoC$.
        Thus,
          $\eval{\varphi}_\rhoA \leqAI_{\aty_\rhoC} \eval{\psi}_\rhoC$ holds.
        \QED
        %}}}
    \end{itemize}
  \end{proof*}%}}}
  \begin{Lemma}
    Suppose that
        $\AbstTyEnv \vdash \varphi
          :        (\tilde{P}, \sigma )
          \subType (\tilde{Q}, \sigma')
          \absInto \varphi'$.
    Let $v'_1,...,v'_l \in \{\vtrue,\vfalse\}$ and assume
      $v'_i \sqsubseteq_\tBool \eval{Q_i}_\rhoC$ for each $i$.
    There exist $v_1,...,v_k \in \{\vtrue,\vfalse\}$ such that
    \begin{itemize}
      \item $v_i \sqsubseteq_\tBool \eval{P_i}_\rhoC$ for each $i$ and
      \item
        $\gamma_{\aty'}\left(
           \eval{(\AbstTyEnv\ctxSep\tilde{Q})^\flat\vdash\varphi':\aty'^\flat}_{
            \rho\cup\{\tilde{b}_Q\mapsto\tilde{v'}\}
           }\right)
        \sqsubseteq_{\aty^\sharp}
        \gamma_{\aty}\left(
          \eval{(\AbstTyEnv\ctxSep\tilde{P})^\flat\vdash\varphi:\aty^\flat}_{
            \rho\cup\{\tilde{b}_P\mapsto\tilde{v}\}
          }\right)$.
    \end{itemize}
  \end{Lemma}
  \begin{proof*}%{{{
    Induction on the derivation with case analysis on the last rule used.
    We show only the case of \inflabel{AC-Base} since other cases are easy.
    Let $v_i = \eval{P_i}_\rhoC$.  We have
    \begin{align*}
      \eval{\varphi'}_{\rho\cup\{\tilde{b}_Q\mapsto\tilde{v'}\}}
      &=
      \eval{\xi}_{\{X\mapsto\eval{\lambda\tilde{b}_P.\varphi}_\rho\}}\tilde{v'}
      \\&\sqsubseteq_\tBool
      \eval{\xi}_{\{X\mapsto\eval{\lambda\tilde{b}_P.\varphi}_\rho\}}\eval{\tilde{Q}}_\rhoC
      \\&\sqsubseteq_\tBool
      \eval{X}_{\{X\mapsto\eval{\lambda\tilde{b}_P.\varphi}_\rho\}}\eval{\tilde{P}}_\rhoC
      \\&=
      \eval{\varphi}_{\rho\cup\{\tilde{b}_P\mapsto\tilde{v}\}}
    \end{align*}
    Thus, required conditions hold. \QED
  \end{proof*}%}}}
\par
  Now we can prove soundness.
  \begin{Theorem*}[Soundness of predicate abstraction]
    Suppose that $\vdash D_\psi : \AbstTyEnv \absInto D_\varphi$ holds.
    If $D_\varphi$ is valid, so is $D_\psi$.
  \end{Theorem*}
  \begin{proof*}
    When $\vdash D_\psi : \AbstTyEnv \absInto D_\varphi$ holds, we have
    $\emptyset \ctxSep \varepsilon \vdash \FromHES{D_\psi} : \tBool \absInto \FromHES{D_\varphi}$
    (by induction on the length of $D_\psi$). Thus, we have
    \begin{align*}
      \eval{D_\varphi} = \eval{\FromHES{D_\varphi}} \leqAI_{\tBool}
      \eval{\FromHES{D_\psi}} = \eval{D_\psi}
    \end{align*}
    Therefore, if $D_\varphi$ is valid, so is $D_\psi$.  \QED
  \end{proof*}
