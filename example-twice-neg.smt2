(set-logic HORN)
(declare-fun A (Int Int) Bool)
(declare-fun C (Int Int) Bool)
(declare-fun D (Int Int) Bool)
(declare-fun F (Int Int) Bool)
(declare-fun H (Int Int) Bool)
(declare-fun B (Int) Bool)
(declare-fun E (Int) Bool)
(declare-fun G (Int) Bool)

(assert (forall ((x Int))
  (=> (G x) (E x))))
(assert (forall ((x Int) (y Int))
  (=> (and (G x) (D x y)) (H y x))))

(assert (forall ((x Int) (y Int))
  (=> (H x y) (E x))))

(assert (forall ((x Int) (y Int) (z Int))
  (=> (and (H x y) (D x z)) (F z y))))

(assert (forall ((x Int))
  (=> (B x) (A x (- x)))))

(assert (forall ((x Int) (y Int))
  (=> (C x y) (= x y))))

(assert (forall ((x Int))
  (=> true (G x))))

(assert (forall ((x Int))
  (=> (E x) (B x))))

(assert (forall ((x Int) (y Int))
  (=> (F x y) (C y x))))

(assert (forall ((x Int) (y Int))
  (=> (and (E x) (A x y)) (D x y))))

(check-sat)
(get-model)

