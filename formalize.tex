\documentclass{bxjsarticle}

% preamble {{{
\usepackage{qtree}
\usepackage{bcprules}
\usepackage{proof}
\usepackage{bussproofs}
\usepackage{color}
\usepackage{amssymb,amsmath}
\usepackage{amssymb}
\usepackage{ascmac}
\usepackage{framed}
\usepackage{stmaryrd} % llbracket
\usepackage{xspace}

\usepackage[amsthm,framed]{ntheorem}
  \theoremindent1\jsZw
  \makeatletter
    \newtheoremstyle{plainindent}%
      {\item[\hskip-\theoremindent\hskip\labelsep\relax\theorem@headerfont##1\ ##2\theorem@separator]}%
      {\item[\hskip-\theoremindent\hskip\labelsep \theorem@headerfont ##1\ ##2\ (##3)\theorem@separator]}
    \newtheoremstyle{nonumberplainindent}%
      {\item[\hskip-\theoremindent\theorem@headerfont\hskip\labelsep ##1\theorem@separator]}%
      {\item[\hskip-\theoremindent\theorem@headerfont\hskip \labelsep ##1\ (##3)\theorem@separator]}
    \renewtheoremstyle{change}%
      {\item[\hskip\labelsep \theorem@headerfont ##1\ ##2\theorem@separator]}%
      {\item[\hskip\labelsep \theorem@headerfont ##1\ ##2\ (##3)\theorem@separator]}
    \renewtheoremstyle{nonumberchange}%
      {\item[\hskip\labelsep \theorem@headerfont ##1\ \theorem@separator]}%
      {\item[\hskip\labelsep \theorem@headerfont ##1\ \ (##3)\theorem@separator]}
  \makeatother
  %theorem environments
  \theoremstyle{plainindent}
  \theorembodyfont{\normalfont}
  \theoremseparator{.}
    \newtheorem{Problem}{Problem}[section]
    \newtheorem{Definition}{Definition}[section]
    \newtheorem{Theorem}{Theorem}[section]
    \newtheorem{Thesis}{Thesis}[section]
    \newtheorem{Lemma}{Lemma}[section]
    \newtheorem{Proof}{Proof}[section]
  \theoremseparator{}
    \renewtheorem*{Problem*}{Problem}[section]
    \renewtheorem*{Definition*}{Definition}[section]
    \renewtheorem*{Theorem*}{Theorem}[section]
    \renewtheorem*{Thesis*}{Thesis}[section]
    \renewtheorem*{Lemma*}{Lemma}[section]
    \renewtheorem*{Proof*}{Proof}[section]
    \newtheorem{Notation}{Notation}[section]
  \theoremstyle{change}
    \newtheorem{Note}{Note}[section]
    \renewtheorem*{Note*}{Note}[section]
    \newtheorem{Remark}{Remark}[section]
    \renewtheorem*{Remark*}{Remark}[section]
    \renewtheorem{proof}{Proof}[section]
    \renewtheorem*{proof*}{Proof}[section]

\newcommand{\memo}[1]{}
\newcommand{\id}[1]{#1}
\newcommand{\nuHFLz}{$\nu\mathrm{HFL}_\mathbb{Z}$\xspace}
\newcommand{\true}{t\!t}
\newcommand{\false}{f\!\!f}
\newcommand{\tBool}{\mathrm{o}}
\newcommand{\tInt}{\mathrm{int}}
\newcommand{\negSym}[1]{\overline{#1}}
\newcommand{\set}[2]{\{\;#1\mid#2\;\}}
\newcommand{\dom}[1]{\mathrm{dom}(#1)}
\newcommand{\arity}[1]{\mathrm{arity}(#1)}
\newcommand{\sort}[1]{\mathrm{sort}(#1)}
\newcommand{\valuation}{\rho}
\newcommand{\aty}{\sigma}
\newcommand{\tyEnv}{\Gamma}
\newcommand{\RefTyEnv}{\Delta}
\newcommand{\ctxSep}{\;|\;}
\newcommand{\predSet}{\Theta}
\newcommand{\absInto}{\leadsto}
\newcommand{\abstPred}[2]{\llfloor #2 \rrfloor_{#1}}
\newcommand{\subType}{\preceq}
\newcommand{\eval}[1]{\llbracket #1 \rrbracket}
\newcommand{\iffdef}{\mathrel{\mathop{\raisebox{1pt}{\scriptsize$:$}} }\Leftrightarrow}

\newcommand{\leqAI}{\dot{\sqsubseteq}}
\newcommand{\rhoA}{\rho_{\mathcal{A} }}
\newcommand{\rhoC}{\rho_{\mathcal{C} }}
\newcommand\SHP[2]{\mathrm{SHP}(#1,#2)}
%}}}

\begin{document}%{{{

\section{Abstraction Relation}

\begin{Definition}[Abstraction type]
  \begin{align*}
    \aty ::= \tBool \mid x:\tInt[P_1,...,P_k]\to\aty \mid \aty_1\to\aty_2
  \end{align*}
\end{Definition}

\begin{Definition}[Abstraction Relation]
  Abstraction relation $\tyEnv \ctxSep \predSet \vdash \psi : \sigma \absInto \varphi$ is defined by the following rules.
  Here, $\tyEnv$ is a abstraction type environment and $\predSet$ is a sequence of predicates (or first-order formulas).
\end{Definition}

% Abstraction Relation {{{
\infrule[Var]{
  }{
    \tyEnv, x:\aty \ctxSep \predSet
      \vdash x:\aty
      \absInto x
  }
\infrule[Base]{
    p(\tilde{a}) \in \predSet
  }{
    \tyEnv \ctxSep \predSet
      \vdash p(\tilde{a}): \tBool
      \absInto b_{p(\tilde{a})}
  }
\infrule[IntApp]{
    \tyEnv \ctxSep \predSet
      \vdash \psi : (x:\tInt[P_1,...,P_k]\to\aty)
      \absInto \varphi
  \andalso
    \tyEnv \vdash_{\mathrm{ST}} a: \tInt
  }{
    \tyEnv \ctxSep \predSet, \tilde{P}[a/x]
      \vdash \psi a : \aty[a/x]
      \absInto \varphi P_1[a/x] \cdots P_k[a/x]
  }
\infrule[IntAbs]{
    \tyEnv,x:\tInt \ctxSep \predSet,\tilde{P}
      \vdash \psi : \aty
      \absInto \varphi
  }{
    \tyEnv \ctxSep \predSet
      \vdash \lambda x. \psi : (x:\tInt[\tilde{P}]\to\aty)
      \absInto \lambda \tilde{b}_{P[a/x]}. \varphi
  }
\infrule[App]{
    \tyEnv \ctxSep \predSet
      \vdash \psi_1 : \aty_1\to\aty_2
      \absInto \varphi_1
  \andalso
    \tyEnv \ctxSep \predSet
      \vdash \psi_2 : \aty_1
      \absInto \varphi_2
  }{
    \tyEnv \ctxSep \predSet
      \vdash \psi_1\psi_2 : \aty_2
      \absInto \varphi_1\varphi_2
  }
\infrule[Abs]{
    \tyEnv,x:\aty_1 \ctxSep \predSet
      \vdash \psi : \aty_2
      \absInto \varphi
  }{
    \tyEnv \ctxSep \predSet
      \vdash \lambda x. \psi : \aty_1\to\aty_2
      \absInto \lambda x. \varphi
  }
\infrule[Mu]{
    \tyEnv,x:\aty \ctxSep \predSet
      \vdash \psi : \aty
      \absInto \varphi
  }{
    \tyEnv \ctxSep \predSet
      \vdash \mu x.\psi : \aty
      \absInto \mu x. \varphi
  }
\infrule[And]{
    \tyEnv \ctxSep \predSet
      \vdash \psi_i : \tBool
      \absInto \varphi_i
  }{
    \tyEnv \ctxSep \predSet
      \vdash \psi_1\land\psi_2
      \absInto \varphi_1\land\varphi_2
  }
\infrule[Or]{
    \tyEnv \ctxSep \predSet
      \vdash \psi_i : \tBool
      \absInto \varphi_i
  }{
    \tyEnv \ctxSep \predSet
      \vdash \psi_1\lor\psi_2
      \absInto \varphi_1\lor\varphi_2
  }
\infrule[Coerce]{
    \tyEnv \ctxSep \predSet
      \vdash \psi : \aty
      \absInto \varphi
  \andalso
    \tyEnv
      \vdash \varphi
      :         (\predSet, \aty )
      \subType  (\predSet',\aty')
      \absInto \varphi'
  }{
    \tyEnv \ctxSep \predSet'
      \vdash \psi : \aty'
      \absInto \varphi'
  }
\infrule[CBase]{
    X:\tBool^k\to\tBool \vdash \xi : \tBool^l\to\tBool \\
    \eval{\xi Q_1...Q_l}_\valuation \sqsubseteq_{\tBool}
    \eval{X   P_1...P_k}_\valuation \text{ for all } \valuation
    % FV(\varphi) \subseteq b_{Q_1}...b_{Q_l} \andalso
  \andalso
  }{
    \tyEnv
      \vdash   \varphi
      :        (P_1,...,P_k, \tBool)
      \subType (Q_1,...,Q_l, \tBool)
      \absInto (\xi b_{Q_1}...b_{Q_l})[\lambda b_{P_1}...b_{P_k}.\varphi/X]
  }
\infrule[CIntArrow]{
    \tyEnv
      \vdash   \varphi b_{P_1}...b_{P_k}
      :        (\predSet_1\cup\{P_1,...,P_k\}, \aty_1)
      \subType (\predSet_2\cup\{Q_1,...,Q_l\}, \aty_2)
      \absInto \varphi'
  \andalso
  }{
    \tyEnv
      \vdash   \varphi
      :        (\predSet_1, x:\tInt[P_1,...,P_k]\to\aty_1)
      \subType (\predSet_2, x:\tInt[Q_1,...,Q_l]\to\aty_2)
      \absInto \lambda b_{Q_1}\cdots b_{Q_l}. \varphi'
  }
\infrule[CArrow]{
    \tyEnv
      \vdash   x
      :        (\emptyset, \aty_1')
      \subType (\predSet', \aty_1 )
      \absInto \varphi_1
  \andalso
    \tyEnv
      \vdash   \varphi \varphi_1
      :        (\predSet\cup\predSet', \aty_2)
      \subType (\predSet', \aty_2')
      \absInto \varphi_2'
  }{
    \tyEnv
      \vdash   \varphi
      :        (\predSet , \aty_1 \to\aty_2 )
      \subType (\predSet', \aty_1'\to\aty_2')
      \absInto \lambda x. \varphi_2'
  }
% }}}

\section{Predicate Discovery}

\begin{Definition}[Refinement type system for \nuHFLz]
  （小林先生が書かれたものと同一です．）
  \newcommand\form{\varphi}
  \newcommand\p{\vdash}
  \newcommand\Form[1]{\mathit{Rty}(#1)}
  \[
    \begin{array}{l}
      \tau   ::= \form
            \mid x:\tInt \to \tau
            \mid \sigma\to\tau\\
      \sigma ::= \tau_1\land\cdots\land \tau_k\\
      \form  ::= \true\mid \false\mid a_1\ge a_2\mid \form_1\land\form_2\mid \form_1\lor\form_2
    \end{array}
  \]
  \begin{align*}
    \Form{\varphi} = \varphi; \quad
    \Form{x:\tInt \to \tau} = \Form{\tau}; \quad
    \Form{\sigma\to\tau} = \Form{\tau}
  \end{align*}
  \infrule{\RefTyEnv(x)=\tau}{\RefTyEnv;\varphi \p x:\tau}
  \infrule{}{\RefTyEnv;\varphi\p a_1\ge a_2: a_1\ge a_2}
  \infrule{\RefTyEnv;\varphi \p \psi : x:\tInt\to \tau}
          {\RefTyEnv;\varphi \p \psi\,a: [a/x]\tau}
  \infrule{\RefTyEnv;\varphi\p\psi_1 : \sigma\to\tau\andalso \RefTyEnv;\varphi\p \psi_2:\sigma}
          {\RefTyEnv;\varphi\p \psi_1\psi_2:\tau}
  \infrule{\RefTyEnv;\varphi \p \psi_1 : \varphi_1\andalso \RefTyEnv;\varphi\p \psi_2:\varphi_2}
          {\RefTyEnv;\varphi\p \psi_1\land\psi_2:\varphi_1\land\varphi_2}
  \infrule{\RefTyEnv;\varphi \p \psi_1 : \varphi_1\andalso \RefTyEnv;\varphi\p \psi_2:\varphi_2}
          {\RefTyEnv;\varphi\p \psi_1\lor\psi_2:\varphi_1\lor\varphi_2}
  \infrule{\RefTyEnv,x:\tau;\varphi \p \psi : \tau}
          {\RefTyEnv;\varphi\p \nu x.\psi:\tau}
  \infrule{\RefTyEnv;\varphi \p \psi : \tau \andalso \RefTyEnv;\varphi\p \tau\subType \tau'}
          {\RefTyEnv;\varphi\p \psi:\tau'}
  \infrule{\RefTyEnv,x:\tInt;\varphi\p \psi:\tau}
          {\RefTyEnv;\varphi\p \lambda x.\varphi: (x:\tInt\to\tau)}
  \infrule{\RefTyEnv,x:\sigma;\varphi\p \psi:\tau}
          {\RefTyEnv;\varphi\p \lambda x.\psi: \sigma\to\tau}
  \infrule{\RefTyEnv;\varphi\p \psi:\tau_i \mbox{ for $i=1,\ldots,k$}}
          {\RefTyEnv;\varphi\p \psi:\tau_1\land\cdots \land\tau_k}
  \infrule{\varphi \land \varphi_2\implies \varphi_1}
          {\RefTyEnv;\varphi\p \varphi_1\subType\varphi_2}
  \infrule{\RefTyEnv;\varphi \p \tau\subType\tau'}
          {\RefTyEnv;\varphi \p x:\tInt\to \tau\subType x:\tInt\to\tau'}
  \infrule{\RefTyEnv;\varphi\land \Form{\tau_2'}\p  \sigma_1'\subType\sigma_1
            \andalso
           \RefTyEnv;\varphi\p \tau_2\subType\tau_2'}
          {\RefTyEnv;\varphi \p \sigma_1\to \tau_2\subType \sigma_1'\to\tau_2'}
\end{Definition}

\newcommand{\cex}{c}
\begin{Definition}[Counterexample]\quad\\
  Raw-counterexample
    $\cex ::= \bot \mid \land_i \cex \mid \cex \lor \cex$.
  Raw-counterexample $\cex$ is called a counterexample of \nuHFLz $\psi$
    if there is a finite expansion $\psi'$ of $\psi$ with
    $\cex \sqsubseteq_c \psi' \succ P$ for some $P$:
  \begin{align*}
    \bot         &\sqsubseteq_c P                 \succ P
        & \text{if $P$ is first-order formula} \\
    % \bot         &\sqsubseteq_c \psi              \succ \true
    %     & \\
    \land_i c    &\sqsubseteq_c \psi_1\land\psi_2 \succ P
        & \text{if $c  \sqsubseteq_c \psi_i \succ P$}\\
    c_1 \lor c_2 &\sqsubseteq_c \psi_1\lor \psi_2 \succ P_1\lor P_2
        & \text{if $c_i\sqsubseteq_c \psi_i \succ P$}\\
  \end{align*}
  % νLoop.λx.Loop xみたいなものはtrueになるから反例にならない
\end{Definition}

\begin{Definition}[SHP]
  Assume there is a finite expansion $\psi'$ of $\psi$ with $\cex \sqsubseteq_c \psi' \succ P$.
  One can construct fixpoint-free \nuHFLz
  $\psi_c = \{ F_0 = \psi_0
             , F_1 \tilde{x_1} = \psi_1
             , ...
             , F_n \tilde{x_n} = \psi_k \}$ such that
  \begin{itemize}
    \item
      $\eval{\psi_c}_\valuation = \eval{P}_\valuation$ for all $\valuation$.
    \item
      Each $\psi_i$ is $\lambda$- and $\land$-free.
    \item
      Each function parameter (including $F_i$) is called at most once
      in the reduction sequence $F_0 \to \cdots \to \psi' \not\to$.
  \end{itemize}
  We write $\SHP{\psi}{c}$ to denote this $\psi_c$.
  \footnote{
    $P$が$\psi'$に依らないことを示す必要が出てくるので有限展開$\psi'$ではなく
    HORSのvalue treeのようなものを考えた方が良いかも知れない？
  }
\end{Definition}

\begin{Definition}[Predicate Discovery]\quad\\
  Input: $\psi_c = \SHP{\psi}{c}$ which is valid \\
  Output: Refinement type environment $\tyEnv$ such that
          (i)  $\RefTyEnv(F_0) = \true$ and
          (ii) $\RefTyEnv;\true \vdash \lambda \tilde{x_i}. \psi_i : \RefTyEnv(F_i)$ for each $i$.
\end{Definition}

アルゴリズムについて：https://bitbucket.org/NaokiIwayama/hflmc2-doc/src/master/inference.pdf

% NOTE: 鈴木さんはSHPを作らずに定式化している

\section{Soundness (of predicate abstraction)}

\begin{Theorem*}[健全性]
  $\tyEnv \ctxSep \predSet \vdash \psi : \sigma \absInto \varphi$かつ
  $\rhoA \;\leqAI_{\tyEnv,\predSet}\; \rhoC$ならば
  $\eval{\tyEnv^\flat \vdash \varphi : \aty^\flat}_{\rhoA}
    \;\leqAI_{\aty_{\rhoC}}\;
   \eval{\tyEnv^\sharp \vdash \psi : \aty^\sharp}_{\rhoC}$
\end{Theorem*}
ここで
\begin{align*}
  w \;\leqAI_{\sigma}\; v
    &\iffdef w         \sqsubseteq_{\sigma^\flat}  \alpha(v)
    \iff    \gamma(w) \sqsubseteq_{\sigma^\sharp} v
    \\
  \rhoA \;\leqAI_{\tyEnv,\predSet}\; \rhoC
    &\iffdef
    \forall x:\aty \in \tyEnv.
        \rhoA(x)   \;\leqAI_{\sigma}\;  \rhoC(x)
    \\
    &\;\;\land
    \forall P \in \predSet.
        \rhoA(b_P) \sqsubseteq_{\tBool} \eval{\tyEnv^\sharp \vdash P : \tBool}_{\rhoC}
\end{align*}
abstraction function $\alpha$と concretization function $\gamma$は鈴木さんの修論と同じものを使う．
証明は未完成だが，(CBase)と(IntApp)の規則については帰納法が回ることを確認した．

% \section{Progress}

\end{document}

%}}}

